import os
import platform
import ConfigParser
import io
import time
import subprocess
import re
import json
import pprint
from itertools import islice

try:
	import readline
except ImportError:
	import pyreadline as readline

def cscreen():
	# input a check to allow clearing screens of both
	# Windows, Mac OS X and Linux terminals
	operatingsystem = platform.system()

	if operatingsystem == 'Windows':
		clear = lambda: os.system('cls')
	else:
		clear = lambda: os.system('clear')

	clear()
	
	#tmp = subprocess.call('clear',shell=True)

def create_cfgfile(config, run_ver):
	# create the configuration file as it doesn't exist yet
	cfgfile = open(config, 'w')
	# add content to the file
	Config = ConfigParser.ConfigParser()
	Config.add_section('program')
	Config.set('program', 'version', run_ver)
	Config.add_section('paths')
	Config.set('paths', 'templates', './Templates/')
	Config.set('paths', 'scripts', './Scripts/')
	Config.set('paths', 'generated', './Generated/')
	Config.add_section('wtg')
	Config.set('wtg', 'addr_per_subnet', 30)
	Config.set('wtg', 'addr_per_block', 8)
	Config.set('wtg', 'vlan_offset', 21)
	Config.add_section('mc')
	Config.set('mc', 'addr_per_subnet', 160)
	Config.set('mc', 'addr_per_block', 1)
	Config.set('mc', 'vlan_offset', 2001)
	Config.add_section('vpc')
	Config.set('vpc', 'addr_per_subnet', 160)
	Config.set('vpc', 'addr_per_block', 1)
	Config.set('vpc', 'vlan_offset', 2011)
	Config.add_section('probe')
	Config.set('probe', 'addr_per_subnet', 160)
	Config.set('probe', 'addr_per_block', 1)
	Config.set('probe', 'vlan_offset', 2021)
	Config.add_section('ppc')
	Config.set('ppc', 'addr_per_block', 20)
	Config.set('ppc', 'addr_offset', 10)
	Config.set('ppc', 'rep_seg_offset', 111)

	# write to file and close it
	Config.write(cfgfile)
	cfgfile.close()

def load_cfgfile(config):

	# load the configuration file
	with open(config) as f:
		read_config = f.read()

	config = ConfigParser.RawConfigParser(allow_no_value=True)
	config.readfp(io.BytesIO(read_config))

	return config

def cfgfile(filename, run_ver):
	done = False
	while done == False:
		if not os.path.isfile(filename):
			# the file does not exist - create it so it can be parsed at next loop
			create_cfgfile(filename, run_ver)
		else:
			# the file exists - verify whether it is the proper version or not

			# load the cfg file and check the version
			cfg = load_cfgfile(filename)
			try:
				cfg_ver = cfg.get('program', 'version')

				if cfg_ver == run_ver:
					# the cfg file version matches the program
					done = True
				else:
					# the cfg file needs to be removed and recreated
					os.remove(filename)
			except:
				os.remove(filename)

	return cfg

def folder_exists(path):
	# ensures a given folder exists
	if not os.path.isdir(path):
		# the directory does not exist
		# create it
		os.mkdir(path)

def get_wtg_info(cfg):
	# set wtg_info for use with WTG calculations
	wtg_info = {}
	wtg_info['wtg_per_subnet'] = cfg.get('wtg', 'addr_per_subnet')
	wtg_info['wtg_addr_per_block'] = cfg.get('wtg', 'addr_per_block')
	wtg_info['wtg_vlan_offset'] = cfg.get('wtg', 'vlan_offset')
	wtg_info['mc_per_subnet'] = cfg.get('mc', 'addr_per_subnet')
	wtg_info['mc_addr_per_block'] = cfg.get('mc', 'addr_per_block')
	wtg_info['mc_vlan_offset'] = cfg.get('mc', 'vlan_offset')
	wtg_info['vpc_per_subnet'] = cfg.get('vpc', 'addr_per_subnet')
	wtg_info['vpc_addr_per_block'] = cfg.get('vpc', 'addr_per_block')
	wtg_info['vpc_vlan_offset'] = cfg.get('vpc', 'vlan_offset')
	wtg_info['probe_per_subnet'] = cfg.get('probe', 'addr_per_subnet')
	wtg_info['probe_addr_per_block'] = cfg.get('probe', 'addr_per_block')
	wtg_info['probe_vlan_offset'] = cfg.get('probe', 'vlan_offset')

	return wtg_info

def get_ppc_info(cfg):
	# set ppc_info for use with PPC calculations
	ppc_info = {}
	ppc_info['addr_per_block'] = cfg.get('ppc', 'addr_per_block')
	ppc_info['addr_offset'] = cfg.get('ppc', 'addr_offset')
	ppc_info['rep_seg_offset'] = cfg.get('ppc', 'rep_seg_offset')

	return ppc_info

def choose_device_category(path):

	# populate a list with all the available
	# device categories to choose from
	device_cat_list = []
	for x in os.listdir(path):
		if os.path.isdir(os.path.join(path, x)):
			device_cat_list.append(x)

	# get choice from user input
	done = False
	while done == False:
		print('Available device categories')
		print('---------------------------')

		for each in sorted(device_cat_list):
			print('- ' + each)

		print('')
		choice = raw_input('Select device category: ')

		if str.upper(choice) in device_cat_list:
			return str(choice)
		elif choice == "":
			# empty choice
			cscreen()
		else:
			cscreen()
			print('Invalid selection. Please refer to the items in the list below...')
			time.sleep(3)

def choose_device_platform(path, category, opt=False):

	# populate a list with all the available
	# device platforms to choose from
	path = path + category + '/'
	device_plat_list = []
	for x in os.listdir(path):
		if os.path.isdir(os.path.join(path, x)):
			device_plat_list.append(x)

	# get choice from user input
	done = False
	while done == False:
		if opt == True:
			print('Available sections for ' + str.upper(category))
		else:
			print('Available platforms for ' + str.upper(category))
		
		print('------------------------------------')

		for each in sorted(device_plat_list):
			print('- ' + each)

		print('')
		if opt == True:
			choice = raw_input('Select option section: ')
		else:
			choice = raw_input('Select device platform: ')

		if str.upper(choice) in device_plat_list:
			return str(choice)
		elif choice == "":
			# empty choice
			cscreen()
		else:
			cscreen()
			print('Invalid selection. Please refer to the items in the list below...')
			time.sleep(1)

def choose_device_version(path, platform):

	# populate a list with all the available
	# platform versions to choose from
	path = path + platform + '/'
	path_platform = path
	platform_vers_list = []
	for version_dir in os.listdir(path):
		if os.path.isdir(os.path.join(path, version_dir)):
			platform_vers_list.append(str.lower(version_dir))

	# get choice from user input
	done = False
	while done == False:
		print('Available versions for ' + str.upper(platform))
		print('------------------------------------')
		regex_ver = '^version([0-9]{1,3})[_]{0,1}([0-9]{0,3})[_]{0,1}([0-9]{0,3})$'
	
		for each in platform_vers_list:
			match = re.search(regex_ver, each, re.IGNORECASE)
			if match:
				display_ver = 'v' + match.group(1)
				if match.group(2): display_ver = display_ver + '.' + match.group(2)
				if match.group(3): display_ver = display_ver + '.' + match.group(3)
				print('- ' + display_ver)

		print('')
		choice = raw_input('Select platform version: ')

		# validate that the input is properly formatted using regex
		match = re.search('([0-9]{1,3})[\._]{0,1}([0-9]{0,3})[\._]{0,1}([0-9]{0,3})$', choice, re.IGNORECASE)
		if match:
			# set the readable and choice variables
			readable = match.group(1)
			choice = 'version' + match.group(1)
			if match.group(2):
				readable = readable + '.' + match.group(2)
				choice = choice + '_' + match.group(2)
			if match.group(3):
				readable = readable + '.' + match.group(3)
				choice = choice + '_' + match.group(3)

			# validate whether the input is on the version list
			validate = ''
			path = path_platform + '/' + choice
			if choice in platform_vers_list:
				validate = check_subconfigs(path)
			else:
				choice = 'version' + match.group(1) + '.' + match.group(2) + '.' + match.group(3)
				if choice in platform_vers_list:
					validate = check_subconfigs(path)
				else:
					print("Something went terribly wrong :(")

			if validate == 'pass':
				return choice, readable
			elif validate == 'fail':
				print('There are no subconfigs available for this platform.')
				raw_input('')
				cscreen()
			else:
				print('An unknown error occurred while validating the version choice.')
				raw_input('')
				cscreen()

		elif choice == "":
			# empty choice
			cscreen()
		else:
			cscreen()
			print('Invalid selection. Please refer to the items in the list below...')

def choose_option_version(path, platform):

	# populate a list with all the available
	# platform versions to choose from
	path = path + platform + '/'
	path_orig = path
	option_vers_list = []
	for version_dir in os.listdir(path):
		if os.path.isdir(os.path.join(path, version_dir)):
			option_vers_list.append(str(version_dir))

	# get choice from user input
	done = False
	while done == False:
		print('Available options in ' + str.upper(platform) + ' section')
		print('------------------------------------')
		regex_ver = '^([A-Za-z0-9_-]*)_version([0-9]{1,3})[_]{0,1}([0-9]{0,3})$'
	
		for each in sorted(option_vers_list):
			match = re.search(regex_ver, each, re.IGNORECASE)
			if match:
				display_name = match.group(1)
				display_ver = 'v' + match.group(2)
				if match.group(2): display_ver = display_ver + '.' + match.group(3)
				print('- ' + display_name + ' ' + display_ver)

		print('')
		choice = raw_input('Select option: ')

		# validate that the input is properly formatted using regex
		match = re.search('^([A-Za-z0-9_\-]*) v([0-9]{1,3})[_\.]{0,1}([0-9]{0,3})$', choice, re.IGNORECASE)
		if match:
			# set the readable and choice variables
			chosen_ver = match.group(2) + '.' + match.group(3)
			replace_ver = chosen_ver.replace('.', '_')
			replace_name = str.upper(match.group(1))
			
			readable = match.group(1) + ' v' + chosen_ver
			
			choice = replace_name + '_version' + replace_ver

			# validate whether the input is on the version list
			validate = ''
			path = path + choice + '/'
			if choice in option_vers_list:
				validate = check_subconfigs(path, True)
			else:
				print('Something went terribly wrong :(')

			if validate == 'pass':
				return choice, readable
			elif validate == 'fail':
				print('There are no subconfigs available for this option.')
				raw_input('')
				cscreen()
			else:
				print('An unknown error occurred while validating the option choice.')
				raw_input('')
				cscreen()

		elif choice == "":
			# empty choice
			cscreen()
		else:
			cscreen()
			print('Invalid selection. Please refer to the items in the list below...')

		path = path_orig

def get_device_locations(template_path):
	done = False
	while done == False:
		chosen_dev_cat = choose_device_category(template_path)

		device_cat_dir = template_path + chosen_dev_cat + '/'

		# set the option_select based on user input
		if str.upper(chosen_dev_cat) == 'OPTIONS':
			option_select = True
		else:
			option_select = False

		chosen_dev_plat = choose_device_platform(template_path, chosen_dev_cat, option_select)

		device_base_dir = device_cat_dir + chosen_dev_plat + '/'

		
		if option_select == True:
			chosen_option_vers, vers_num = choose_option_version(device_cat_dir, chosen_dev_plat)
			vers_dir = device_base_dir + chosen_option_vers + '/'
		else:
			chosen_dev_vers, vers_num = choose_device_version(device_cat_dir, chosen_dev_plat)
			vers_dir = device_base_dir + chosen_dev_vers + '/'

		# present the choices and ask user to confirm
		option_enable = False
		confirmed = False
		while confirmed == False:
			if option_select == True:
				confirmed_enable = False
				while confirmed_enable == False:
					# choose whether to enable or disable the option
					confirm_enable = raw_input('Do you wish to ENABLE or DISABLE the option (' + chosen_option_vers + ')? ')
					confirm_enable = str.upper(confirm_enable)
					
					if confirm_enable == 'ENABLE':
						chosen_enable = confirm_enable
						option_enable = True
						cscreen()
						confirmed_enable = True
					elif confirm_enable == 'DISABLE':
						chosen_enable = confirm_enable
						option_enable = False
						cscreen()
						confirmed_enable = True
					elif confirm_enable == '':
						cscreen()
					else:
						cscreen()
						raw_input('Invalid input...')

				print('You have chosen to ' + chosen_enable + ' the option (' + str.upper(chosen_dev_plat) + '): ' + chosen_option_vers)
			else:
				print('You have chosen a ' + str.upper(chosen_dev_cat) + ' model ' + str.upper(chosen_dev_plat) + ' version ' + vers_num)

			confirmation = raw_input('Do you want to continue with this selection? [Y/N]: ')

			confirmation = str.upper(confirmation)
			if confirmation == 'Y':
				return device_cat_dir, device_base_dir, vers_dir, option_select, option_enable
			elif confirmation == 'N':
				cscreen()
				confirmed = True
			elif confirmation == '':
				cscreen()
			else:
				cscreen()
				raw_input('Invalid input...')

def merge_two_dicts(x, y):
	z = x.copy()   # start with x's keys and values
	z.update(y)    # modifies z with y's keys and values & returns None
	return z

def default_input(prompt, default=''):
	if default != '':
		res = raw_input(prompt + ' [' + str(default) + ']: ')
	else:
		res = raw_input(prompt + ': ')
	
	res = res or default

	return res

def load_device_variables(base_dir, dev_dir, ver_dir):
	device_variables = {}

	var_file_list = []
	var_file_list.append(base_dir + 'variables.json')
	var_file_list.append(dev_dir + 'variables.json')
	var_file_list.append(ver_dir + 'variables.json')

	first = True
	for file in var_file_list:
		if os.path.isfile(file):
			with open(file, 'r') as read_file:
				if first == True:
					device_variables = json.load(read_file)
					first = False
				else:
					loaded = json.load(read_file)
					device_variables = merge_two_dicts(device_variables, loaded)

	return device_variables

def load_device_scripts(base_dir, dev_dir, ver_dir):
	device_scripts = {}

	var_file_list = []
	var_file_list.append(base_dir + 'scripts.json')
	var_file_list.append(dev_dir + 'scripts.json')
	var_file_list.append(ver_dir + 'scripts.json')

	first = True
	for file in var_file_list:
		if os.path.isfile(file):
			with open(file, 'r') as read_file:
				if first == True:
					device_scripts = json.load(read_file)
					first = False
				else:
					loaded = json.load(read_file)
					device_scripts = merge_two_dicts(device_scripts, loaded)

	return device_scripts

def load_work_instructions(base_dir, dev_dir, ver_dir, target_fw):

	work_instr_list = []
	work_instr_list.append(base_dir + 'work_instruction.txt')
	work_instr_list.append(dev_dir + 'work_instruction.txt')
	work_instr_list.append(ver_dir + 'work_instruction.txt')

	work_instructions = '\n\n\n\n'
	cmdline = False
	for file in work_instr_list:
		if os.path.isfile(file):
			with open(file, 'r') as read_file:
				for line in read_file:
					if line.startswith('{{CMDSTART}}'):
						cmdline = True
						line = '! Commands start\n'
					elif line.startswith('{{CMDSTOP}}'):
						cmdline = False
						line = 'Commands end\n'
					elif re.search('{{TARGET_FIRMWARE}}', line):
						line = line.replace('{{TARGET_FIRMWARE}}', target_fw)
					elif re.search('{{VLAN_LIST}}', line):
						# need to fetch VLANs for the work instructions
						# prefer the VLANs in order from
						# 1.) file in version dir
						# 2.) file in device dir
						# 3.) file in base dir
						# Only one file will be used - no merging of configurations.
						valid_file = False
						if os.path.isfile(ver_dir + 'vlans.conf'):
							vlan_file = ver_dir + 'vlans.conf'
							valid_file = True
						elif os.path.isfile(dev_dir + 'vlans.conf'):
							vlan_file = dev_dir + 'vlans.conf'
							valid_file = True
						elif os.path.isfile(base_dir + 'vlans.conf'):
							vlan_file = base_dir + 'vlans.conf'
							valid_file = True
						
						if valid_file == True:
							with open(vlan_file, 'r') as vlan_list:
								vlans = ''
								for vlan_line in vlan_list:
									vlans = vlans + vlan_line
								
								line = vlans
						else:
							line = '!### VLAN LIST MISSING ###!'

					if cmdline == False:
						line = '! ' + line
					work_instructions = work_instructions + line

	return str(work_instructions)

def get_target_firmware(template):
	# look for a boot system sequence in the first 16 lines of the template
	for line in islice(template.splitlines(), 0, 15):
		match = re.search(r'boot system (?:.*):((?:.*)\.bin)', line)
		if match:
			target_fw = str(match.group(1))
			
			return target_fw

	return ''

def display_input_variables(device_variables, sort_order):
	# walk over the contents of the device_variables dict
	for key, value in sorted(sort_order.items()):
		if device_variables[value]['userinput'] == True:
			if 'choice' in device_variables[value]:
				if value == 'INSIDE_FIRST_THREE_OCTETS':
					print(device_variables[value]['longname'] + ' = ' + device_variables[value]['choice'] + '0 /24')
				else:
					print(device_variables[value]['longname'] + ' = ' + device_variables[value]['choice'])
			else:
				print(device_variables[value]['longname'] + ' = ')

def remove_input_variables(device_variables):
	# walk over the contents of the device_variables dict
	for key, value in sorted(device_variables.items()):
		device_variables[key].pop('choice', None)

	return device_variables

def get_variable_data(device_variables, prev_variables=''):
	# skip if there are no device_variables or prev_variables
	if device_variables == '':
		print('No variables to to enter. Skipping...')
		raw_input()
		return device_variables

	# if there are any previous variables then add the choices to the proper places
	prev_vars_exist = False
	if prev_variables != '':
		# iterate over each key in the previous variables dict
		for key, value in prev_variables.items():
			# only look at the keys with userinput required
			if prev_variables[key]['userinput'] == True:
				# check whether or not there is a matching key
				# in the selected device_variables
				if key in device_variables.keys():
					# if there is a previous choice available for the key
					if 'choice' in prev_variables[key].keys():
						# set the device_variables choice accordingly
						device_variables[key]['choice'] = prev_variables[key]['choice']
						prev_vars_exist = True

	# build a dict for sorting purposes
	sort_order = {}
	sort_count = 99900
	for key, value in device_variables.items():
		if 'order' in device_variables[key].keys():
			sort_order[int(device_variables[key]['order'])] = key
		else:
			sort_order[sort_count] = key
			sort_count += 1

	# enter into a loop until choices are made and validated
	done = False
	rechoose = False
	no_userinput = True
	while done == False:
		# walk over the contents of the device_variables dict
		for key, value in sorted(sort_order.items()):
			if device_variables[value]['userinput'] == True:
				no_userinput = False
				cscreen()
				display_input_variables(device_variables, sort_order)
				print('----------------------------------------')
				print('')
				choice_validated = False
				while choice_validated == False:
					# take input from user
					if rechoose == True or prev_vars_exist == True:
						# the template variables are getting rechosen
						if 'choice' in device_variables[value].keys():
							default_value = device_variables[value]['choice']
						elif 'default_value' in device_variables[value].keys():
							# there is a default value assigned
							default_value = device_variables[value]['default_value']
						else:
							default_value = ''
					else:
						if 'default_value' in device_variables[value].keys():
							# there is a default value assigned
							default_value = device_variables[value]['default_value']
						else:
							default_value = ''

					prompt = device_variables[value]['longname']
					choice = default_input(prompt, default_value)
					choice = str(choice)

					# strip any illegal characters from the input
					#choice = re.sub(r'\W+', '', choice)

					# format the input according to well-known values
					if value == 'WTG_NUM':
						# pad the input if it is a WTG number
						if len(choice) == 1:
							choice = '00' + choice
						elif len(choice) == 2:
							choice = '0' + choice
					elif value == 'PARK_NAME':
						# ensure the park name consists of 5 numbers preceded by an 'SP-'
						match = re.search('[0-9]{5}', choice)
						if match:
							choice = 'SP-' + match.group(0)
					elif value == 'INSIDE_FIRST_THREE_OCTETS':
						# ensure the inside subnet is stripped of the fourth octet
						match = re.search(r'\b((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3})(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b', choice)
						if match:
							choice = str(match.group(1))

					# always capitalize input except for value SITE_NAME
					if value == 'SITE_NAME':
						# replace any whitespaces with underscores
						choice = choice.replace(" ", "_")
					elif value == 'NOIP_SITE_PASSWORD':
						# do nothing to the NOIP key
						choice = str(choice)
					elif value == 'IRONDOME_USER' or value == 'IRONDOME_PASSWD' or value == 'IRONDOME_ENABLE':
						# do nothing to the IRONDOME key iputs
						choice = str(choice)
					else:
						# everything else is capitalized
						choice = str.upper(choice)

					# now validate the result
					#print("Matching for choice: " + str(choice))
					#print("Regex string: " + str(device_variables[value]['regex']))
					match = re.search(device_variables[value]['regex'], choice)
					#print("Match is: " + str(match))
					if choice == "":
						print('Description: ' + device_variables[value]['description'])
						print('Example: ' + device_variables[value]['example'])
					elif match:
						if value == 'COUNTRY_CODE':
							# ensure the country code entered is in the countrycodes list
							countrylistfile = './countrycodes.json'
							if os.path.isfile('./countrycodes.json'):
								with open(countrylistfile, 'r') as read_file:
									country_list = json.load(read_file)

							if str(choice) in str(country_list.keys()):
								# set the region based on the country code
								country_name = country_list[choice]['name']
								region = country_list[choice]['region']
								device_variables[value].update( {'choice' : choice} )
								device_variables['REGION'] = {'country_name': country_name, 'userinput': False, 'choice' : region}
								choice_validated = True
							else:
								print('The country code is invalid....')
								print('The countrycode must adhere to the ISO 3166 shortname standard.')
								print('Valid input example: ' + device_variables[value]['example'])

						else:
							device_variables[value].update( {'choice' : choice} )
							choice_validated = True
					else:
						print('Invalid input...')
						print('Valid input example: ' + device_variables[value]['example'])

		if no_userinput == True:
			# there is no input variables
			return device_variables
		else:
			# confirm userinput choices
			confirmed = False
			while confirmed == False:
				cscreen()
				display_input_variables(device_variables, sort_order)
				print('----------------------------------------')
				print('')

				confirmation = raw_input('Accept the current selections [A], rechoose input selections [R] or clear input selections [C]: ')

				confirmation = str.upper(confirmation)
				if confirmation == 'A':
					return device_variables
				elif confirmation == 'R':
					#device_variables = remove_input_variables(device_variables)
					rechoose = True
					confirmed = True
				elif confirmation == 'C':
					device_variables = remove_input_variables(device_variables)
					rechoose = False
					confirmed = True
				elif confirmation == '':
					confirmed = False
				else:
					raw_input('Invalid input...')

def read_file(file, pattern=''):
	# set the default return variable
	file_return = ''

	# verify that the file exists
	if os.path.isfile(file):

		if pattern != '':
			# a custom pattern is supplied
			# override default function
			pattern_begin = pattern + ' begin'
			pattern_end = pattern + ' end'

			commit_line = False
			with open(file, 'r') as readfile:
				for line in readfile:
					if re.search(pattern_begin, line):
						commit_line = True
					elif re.search(pattern_end, line):
						commit_line = False
					else:
						if commit_line == True:
							file_return = file_return + line

		else:
			# read a file and omit any comments in the file
			# comments are identified as starting with
			# this: #--->
			# or this: !--->
			excl_patterns_list = []
			excl_patterns_list.append('!--->')
			excl_patterns_list.append('#--->')

			skip_line = False
			with open(file, 'r') as readfile:
				for line in readfile:
					for excl_pattern in excl_patterns_list:
						if line.startswith(excl_pattern):
							skip_line = True

					if skip_line == False:
						file_return = file_return + line
					else:
						skip_line = False

	return file_return

def check_subconfigs(subconf_dir, opt=False):

	# set the regex string
	if opt == True:
		regex = '(?:enable|disable)\.conf'
	else:
		regex = '[0-9]{1,3} - [A-Za-z0-9,_\- ]*\.conf'

	# check that there are valid subconfig files available
	valid_count = 0
	for subconf_file in sorted(os.listdir(subconf_dir)):
		if re.search(regex, subconf_file):
			valid_count+=1

	if valid_count >= 1 and opt == False: 
		return 'pass'
	elif valid_count >= 2 and opt == True:
		return 'pass' 
	else:
		return 'fail'

def build_subconfigs(base_dir, subconf_dir, opt=False, opt_enable=False):
	if opt:
		# grab proper option file (enable or disable)
		if opt_enable:
			# set the path to the enable config
			path_subconfig_file = subconf_dir + 'enable.conf'
		else:
			# set the path to the disable config
			path_subconfig_file = subconf_dir + 'disable.conf'
		
		# grab the config file
		option_config = read_file(path_subconfig_file)
	else:
		path_baseconf = base_dir + 'base.conf'
		if os.path.isfile(path_baseconf):
			base_config = read_file(path_baseconf)
		else:
			base_config = ''

		sub_configs = ''
		for subconf_file in sorted(os.listdir(subconf_dir)):
			if re.match("(?:[0-9]{1,3}) - (?:[A-Za-z0-9-_].*)\.conf", subconf_file):
			#if subconf_file.endswith(".conf"):
				path_subconfig_file = subconf_dir + subconf_file
				sub_configs = sub_configs + read_file(path_subconfig_file)

	if opt:
		# build for an option template
		config = option_config
	else:
		# build for a regular template
		config = 'tclsh\nputs [open "flash:/config.orig" w] { ! Generated using Vestas Builder\n' + base_config + sub_configs + '\n}\ntclquit'

	return config

def wtg_subnet_calc(wtg_num, dev_type, wtg_info):
	wtg_host = ''
	wtg_vlan = ''
	wtg_gw = ''

	# the first two octets are fixed
	octet1 = 10
	octet2 = 56
	# calculate the WTG subnet based on WTG number
	q, r = divmod((wtg_num-1), int(wtg_info['wtg_per_subnet']))
	octet3 = q + int(wtg_info['wtg_vlan_offset'])
	octet4 = (r + 1) * int(wtg_info['wtg_addr_per_block'])

	if dev_type == 'tow':
		wtg_host = str(octet1) + "." + str(octet2) + "." + str(octet3) + "." + str(octet4)
	elif dev_type == 'nac':
		wtg_host = str(octet1) + "." + str(octet2) + "." + str(octet3) + "." + str(octet4+1)
	elif dev_type == 'hub':
		wtg_host = str(octet1) + "." + str(octet2) + "." + str(octet3) + "." + str(octet4+2)
	elif dev_type == 'vmet':
		wtg_host = str(octet1) + "." + str(octet2) + "." + str(octet3) + "." + str(octet4+3)

	wtg_vlan = str(octet3)
	wtg_gw = str(octet1) + "." + str(octet2) + "." + str(octet3) + ".254"

	return wtg_host, wtg_vlan, wtg_gw

def dev_subnet_calc(wtg_num, wtg_info, device):
	# the first two octets are fixed
	octet1 = 10
	octet2 = 60

	# set the variables according to selected device
	if device == "mc":
		addr_per_subnet = int(wtg_info['mc_per_subnet'])
		addr_per_block = int(wtg_info['mc_addr_per_block'])
		vlan_offset = int(wtg_info['mc_vlan_offset'])
	elif device == "vpc":
		addr_per_subnet = int(wtg_info['vpc_per_subnet'])
		addr_per_block = int(wtg_info['vpc_addr_per_block'])
		vlan_offset = int(wtg_info['vpc_vlan_offset'])
	elif device == "probe":
		addr_per_subnet = int(wtg_info['probe_per_subnet'])
		addr_per_block = int(wtg_info['probe_addr_per_block'])
		vlan_offset = int(wtg_info['probe_vlan_offset'])
	else:
		print('Code error: Invalid device option passed to dev_subnet_calc')

	# calculate the subnet_offset based on the vlan_offset
	subnet_offset = vlan_offset-2000

	# calculate the device subnet based on WTG number
	q, r = divmod((wtg_num-1), addr_per_subnet)
	octet3 = q + subnet_offset
	octet4 = (r + 1) * addr_per_block

	# calculate the device VLAN
	device_vlan = octet3+2000

	device_host_addr = str(octet1) + "." + str(octet2) + "." + str(octet3) + "." + str(octet4)
	device_gateway = str(octet1) + "." + str(octet2) + "." + str(octet3) + ".252"

	return device_host_addr, device_vlan, device_gateway

def ppc_subnet_calc(ppc_num, vopm_num, dev_type, ppc_info):
	# the first two octets are fixed
	octet1 = 10
	octet2 = 56
	octet3 = 1

	addr_per_block = int(ppc_info['addr_per_block'])
	addr_offset = int(ppc_info['addr_offset'])

	octet4 = (addr_per_block*ppc_num)-addr_offset

	if dev_type == "ext":
		octet4 = octet4 + 1
	elif dev_type == "vopm":
		octet4 = int(octet4) + 1 + int(vopm_num)

	device_host_addr = str(octet1) + "." + str(octet2) + "." + str(octet3) + "." + str(octet4)

	return device_host_addr

def build_noninput_vars(variables, dev_type, cfg, ver_dir, opt=False):
	if 'WTG_NUM' in variables.keys():
		# if the variables dict contain a WTG Number variable
		# the following variables must be derived from this number
		# - VL_ID
		# - VL_ADDR
		# - VL_GATEWAY
		# - MC_WTG_HOST
		# - MC_VPC_HOST
		#
		# get the WTG information from the config file
		wtg_info = get_wtg_info(cfg)

		wtg_num = int(variables['WTG_NUM']['choice'])

		try:
			wtg_host, wtg_vlan, wtg_gw = wtg_subnet_calc(wtg_num, dev_type, wtg_info)
			if 'VL_ADDR' in variables.keys():
				variables['VL_ADDR']['choice'] = wtg_host
			if 'VL_ID' in variables.keys():
				variables['VL_ID']['choice'] = wtg_vlan
			if 'VL_GATEWAY' in variables.keys():
				variables['VL_GATEWAY']['choice'] = wtg_gw
		except:
			print('Error calculating WTG information...')
			raw_input()

		try:
			mc_host, mc_vlan, mc_gw = dev_subnet_calc(wtg_num, wtg_info, 'mc')
			if 'MC_WTG_HOST' in variables.keys():
				variables['MC_WTG_HOST']['choice'] = mc_host
			if 'MC_WTG_VLAN' in variables.keys():
				variables['MC_WTG_VLAN']['choice'] = mc_vlan
			if 'MC_WTG_GW' in variables.keys():
				variables['MC_WTG_GW']['choice'] = mc_gw
		except Exception as e:
			print('Error calculating MC information...')
			raw_input()

		try:
			vpc_host, vpc_vlan, vpc_gw = dev_subnet_calc(wtg_num, wtg_info, 'vpc')
			if 'MC_VPC_HOST' in variables.keys():
				variables['MC_VPC_HOST']['choice'] = vpc_host
			if 'MC_VPC_VLAN' in variables.keys():
				variables['MC_VPC_VLAN']['choice'] = vpc_vlan
			if 'MC_VPC_GW' in variables.keys():
				variables['MC_VPC_GW']['choice'] = vpc_gw
		except:
			print('Error calculating VPC information...')
			raw_input()

		try:
			probe_host, probe_vlan, probe_gw = dev_subnet_calc(wtg_num, wtg_info, 'probe')
			if 'MC_PROBE_HOST' in variables.keys():
				variables['MC_PROBE_HOST']['choice'] = probe_host
			if 'MC_PROBE_VLAN' in variables.keys():
				variables['MC_PROBE_VLAN']['choice'] = probe_vlan
			if 'MC_PROBE_GW' in variables.keys():
				variables['MC_PROBE_GW']['choice'] = probe_gw
		except:
			print('Error calculating PROBE information...')
			raw_input()
	
	if 'PPC_NUM' in variables.keys():
		# if the variables dict contain a PPC Number variable
		# the following variables must be derived from this number
		# - VL_ADDR
		#
		# get the PPC information from the config file
		ppc_info = get_ppc_info(cfg)

		if dev_type == 'vopm':
			vopm_num = variables['VOPM_NUM']['choice']
		else:
			vopm_num = 0

		ppc_num = int(variables['PPC_NUM']['choice'])

		ppc_host = ppc_subnet_calc(ppc_num, vopm_num, dev_type, ppc_info)

		variables['VL_ADDR']['choice'] = ppc_host

		if 'REP_NUM' in variables.keys():
			if variables['REP_NUM']['userinput'] == False:
				# the REP_NUM needs to be calculated
				rep_offset = int(ppc_info['rep_seg_offset'])
				variables['REP_NUM']['choice'] = rep_offset + ppc_num -1

	
	if 'PARK_NUM' in variables.keys():
		# the variables contain the requirement for a PARK_NUM to be calculated
		match = re.search(variables['PARK_NUM']['regex'], variables['PARK_NAME']['choice'])
		if match:
			park_num = int(match.group(0))

		variables['PARK_NUM']['choice'] = park_num

	if 'VER_NUM' in variables.keys():
		# the variables contain the requirement for a VER_NUM to be derived from the ver folder
		
		if opt == True:
			# handle versioning of option template variables
			version_pattern = 'version((?:[0-9]+)_(?:[0-9]+))'
		else:
			# handle versioning of non-option template variables
			version_pattern = 'version((?:[0-9]+)_(?:[0-9]+)_(?:[0-9]+))'
			
		match = re.search(version_pattern, ver_dir)
		if match:
			version_number = match.group(1)
			version_number = version_number.replace('_', '.')
			variables['VER_NUM']['choice'] = version_number

	# handle region specific configs for router deployments
	# building the list of keys that are region-specific
	region_key_list = []
	for key in variables.keys():
		if 'region' in variables[key].keys():
			# there is a region key
			if variables[key]['region'] == True:
				region_key_list.append(key)

	for region_config in region_key_list:
		# for each of the items in the region_key_list
		# grab the configuration if it exists and dump it into the variables choice section
		if region_config in variables.keys():
			# the variables contain the requirements for a region-specific configuration to be retrieved
			region = variables['REGION']['choice']
			file = ver_dir + region + '.conf'

			config = read_file(file, region_config)

			if config != '':
				config = config.rstrip(os.linesep)
				variables[region_config]['choice'] = config

	return variables

def replacer(template, variables):
	return_template = ''
	# run through the variables
	# and build a dict of all the vars and associated values
	answers = {}
	for key, value in variables.items():
		# if there is a choice available then add it to the answers dict
		if 'choice' in variables[key].keys():
			var_name = '{{' + str.upper(str(key)) + '}}'
			var_repl = variables[key]['choice']

			answers[var_name] = var_repl

	# go through the template line by line
	regex_var = '{{[A-Za-z0-9-_].*}}'
	for line in template.splitlines():
		# if the current line contains a variable
		# that needs to be replaced
		match = re.search(regex_var, line, re.IGNORECASE)
		if match:
			# the line contains at least one variable that must be replaced
			linechange = line
			for variable in answers.keys():
				linechange = linechange.replace(str(variable), str(answers[variable]))

			if line == linechange:
				# an error should be shown indicating that
				# a line needed chaning, but was not changed
				print('The following line needs changing, but is unable to be changed due to an error.')
				print('Erroneous line: ' + linechange)
				print('The most likely cause is an improperly named variable in the template.')
				print('')

			line = linechange

		return_template = return_template + line + '\n'

	return return_template

def detect_device_type(sel_base_dir, sel_device_dir):
	# detect the device type based on the path
	device_type = ''

	if re.search(r'/wtg/$', sel_base_dir, re.IGNORECASE):
		# the device is a WTG - detect the subversion
		if re.search(r'-tow/$', sel_device_dir, re.IGNORECASE):
			# this is a tower switch
			device_type = 'tow'
		elif re.search(r'-nac/$', sel_device_dir, re.IGNORECASE):
			# this is a nacelle switch
			device_type = 'nac'
		elif re.search(r'-hub/$', sel_device_dir, re.IGNORECASE):
			# this is a hub switch
			device_type = 'hub'
		elif re.search(r'-vmet/$', sel_device_dir, re.IGNORECASE):
			# this is a vmet switch
			device_type = 'vmet'
	elif re.search(r'/ppc/$', sel_base_dir, re.IGNORECASE):
		# the device is a PPC - detect the subversion
		if re.search(r'core/$', sel_device_dir, re.IGNORECASE):
			# this is a CORE switch
			device_type = 'core'
		elif re.search(r'ext/$', sel_device_dir, re.IGNORECASE):
			# this is an EXT switch
			device_type = 'ext'
		elif re.search(r'vopm/$', sel_device_dir, re.IGNORECASE):
			# this is a VOPM switch
			device_type = 'vopm'

	return device_type

def generate_savefilename(template, opt=False):
	# generate a filename based on hostname lookup in template and a timestamp
	filename = ''

	# generate timestamp
	timestamp = time.strftime("%Y-%m-%d-%H%M%S", time.localtime(time.time()))

	if opt == True:
		regex_name = '!### OPTION: ([A-Za-z0-9_-]*)'
		regex_vers = '!### VERSION: ([0-9]{1,3}\.{0,1}[0-9]{0,3})'
		regex_enable = '!### (ENABLE|DISABLE)'
		option_name = ''
		option_vers = ''
		option_enable = ''

		regex_list = []
		regex_list.append(regex_name)
		regex_list.append(regex_vers)
		regex_list.append(regex_enable)

		# look for info in the first 5 lines of the option template
		for line in islice(template.splitlines(), 0, 4):
			for regex in regex_list:
				match = re.match(regex, line, re.IGNORECASE)
				if match:
					if regex == regex_name:
						option_name = match.group(1)
					elif regex == regex_vers:
						option_vers = match.group(1)
					elif regex == regex_enable:
						option_enable = match.group(1)
		
		filename = 'option_' + timestamp + '_' + option_name + '_vers' + option_vers + '_' + option_enable + '.conf'
		return filename
	else:
		# look for a hostname in the first 16 lines of the template
		for line in islice(template.splitlines(), 0, 15):
			hostname_match = re.match('^hostname (.*)$', line, re.IGNORECASE)
			if hostname_match:
				hostname = str.upper(str(hostname_match.group(1)))
				filename = 'template_' + timestamp + '_' + hostname + '.conf'
				return filename

	return 'ERROR'

def write_template_to_file(path, filename, template):
	# build the path and filename
	savefile_name = path + filename
	try:
		with open(savefile_name, "w") as savefile:
			savefile.write(template)
		return 'Template saved: ' + savefile_name
	except:
		return 'Unable to save file to location ' + savefile_name

def build_scripts(script_path, script_list):
	scripts = ''

	if script_list != '':
		scripts = '\nmkdir flash:/EEM\n\n'

		for key, value in sorted(script_list.items()):
			scripts = scripts + '\ntclsh\nputs [open "flash:/EEM/' + str(script_list[key]['filename']) + '" w] { # ' + str(key) + '\n'

			# load in the script from file
			script_to_load = script_path + str(script_list[key]['filename'])
			if os.path.isfile(script_to_load):
				with open(script_to_load, "r") as read_file:
					for line in read_file:
						scripts = scripts + str(line)
			else:
				scripts = scripts + '# ERROR: unable to locate script file'

			scripts = scripts + '\n}\ntclquit\n'

	return scripts

def main():

	# set base variables for the program
	program_version = '0.10'
	configfile_name = 'config.ini'

	# load in the config file contents
	cfg = cfgfile(configfile_name, program_version)

	# ensure all required directories are created
	for function, path in cfg.items("paths"):
		folder_exists(path)

	# set the template_path to the base directory
	# where the template subdirectories can be located
	cscreen()
	template_path = cfg.get('paths', 'templates')

	finished = False
	while finished == False:
		# get input from user to select the proper device locations
		sel_base_dir, sel_device_dir, sel_vers_dir, option_selection, option_enable = get_device_locations(template_path)

		# identify the specific device switch type
		device_type_detected = detect_device_type(sel_base_dir, sel_device_dir)

		sel_device_variables = load_device_variables(sel_base_dir, sel_device_dir, sel_vers_dir)

		# get input from user to get the proper device variables
		if 'previous_variables' in locals():
			# there are previous variables to account for
			time.sleep(1)
			populated_device_variables = get_variable_data(sel_device_variables, previous_variables)
		else:
			populated_device_variables = get_variable_data(sel_device_variables)

		# fill in non-input variables
		populated_device_variables = build_noninput_vars(populated_device_variables, device_type_detected, cfg, sel_vers_dir, option_selection)

		# gather the required subconf files and
		# build them together with the base.conf
		template = build_subconfigs(sel_base_dir, sel_vers_dir, option_selection, option_enable)

		# build the required scripts for the selected device
		script_path = cfg.get('paths', 'scripts')
		sel_device_scripts = load_device_scripts(sel_base_dir, sel_device_dir, sel_vers_dir)
		scripts = build_scripts(script_path, sel_device_scripts)

		
		if option_selection:
			# do replacements on the option template
			output_template = replacer(template, populated_device_variables)
		else:
			# replace the variables in the template with proper values and
			# add any required TCL scripts at the end
			target_fw = get_target_firmware(template)
			output_template = replacer(template, populated_device_variables) + '\n' + scripts + '\n' + load_work_instructions(sel_base_dir, sel_device_dir, sel_vers_dir, target_fw)

		# save the template to file
		save_path = cfg.get('paths', 'generated')
		filename = generate_savefilename(output_template, option_selection)
		if filename != 'ERROR':
			print(write_template_to_file(save_path, filename, output_template))
		else:
			print('ERROR: no template could be generated.')

		# ask user to repeat or quit
		confirmed = False
		while confirmed == False:
			confirmation = raw_input('Build another template? [Y/N]: ')

			confirmation = str.upper(confirmation)
			if confirmation == 'Y':
				# the user wants to build another template
				previous_variables = populated_device_variables
				time.sleep(1)
				cscreen()
				confirmed = True
			elif confirmation == 'N':
				# the user is done building templates
				time.sleep(1)
				cscreen()
				confirmed = True
				finished = True
			elif confirmation == '':
				cscreen()
			else:
				raw_input('Invalid input...')
				cscreen()

# run the main program
if __name__ == "__main__":
	main()
