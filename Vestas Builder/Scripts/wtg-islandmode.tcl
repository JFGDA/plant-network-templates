::cisco::eem::description "Island Mode allows for changing the service tech ports when limited reachability is detected."
::cisco::eem::event_register_syslog tag down pattern "%TRACK-6-STATE: 1 ip sla 1 reachability Up -> Down" nice 0 queue_priority high maxrun 120
::cisco::eem::event_register_syslog tag up pattern "%TRACK-6-STATE: 1 ip sla 1 reachability Down -> Up"
::cisco::eem::event_register_timer tag reboot cron cron_entry @reboot
::cisco::eem::trigger {
  ::cisco::eem::correlate event down or event up or event reboot
}

########################################################
#
# Script name: wtg-islandmode.tcl
# Version: 1.0.4
#
# Enables or disables the island mode parameters on a
# set of interfaces defined in the config by using
# a mandatory environment variable.
# An error will be thrown before exiting if the
# environment variable is not defined.
#
# Prerequisites:
# - environment variable im_intf must be
#   defined as shown in the following example:
#     event manager environment im_intf Fa1/12 Fa1/11
#	multiple interfaces is supported as shown above
# - track object 1 referencing ip sla 1 must be
#   defined as shown below (example only):
#     ip sla 1
#      icmp-echo 10.56.21.254 source-interface Vlan21
#      threshold 2000
#      timeout 2000
#      frequency 5
#     ip sla schedule 1 life forever start-time now
#     !
#     track 1 ip sla 1 reachability
#      delay down 30 up 30
# - an ACL is expected to be configured and must be
#   name ACL_SERVICE_TECH_IN
#
# IMPORTANT: Commands entered into the variables
# cmds_enable and cmds_disable must be indented
# according to how it is represented in the
# running-config for purposes of verification.
#
########################################################

namespace import ::cisco::eem::*
namespace import ::cisco::lib::*

##### Verify environment variables before proceeding #####

# verify that the required environment variables are set
if {![info exists im_intf]} {
  action_syslog priority 2 msg "Environment variable im_intf is not defined. Please specify the service port interface using the command 'event manager environment im_intf <interface id>' under global configuration mode"
  exit 0
} else {
	# create a list from the environment variable im_intf
	set list_intf [regexp -all -inline {\S+} $im_intf]

	# build a list of valid interfaces
	foreach i $list_intf {
		if ([regexp {(?:[Ff]ast[Ee]thernet|[Gg]igabit[Ee]thernet|[Ff]a|[Gg]i){1}[0-9]{1}\/[0-9]{1,2}} $i matchresult]) {
			lappend list_intf_valid $i
		}
	}

	set list_intf_len [llength $list_intf_valid]

	if {$list_intf_len > 0} {
		# the list contains valid entries
		set counter 0
		foreach intf $list_intf_valid {
			# build a string including separators
			incr counter
			if {$list_intf_len == $counter} {
				append intf_list "$intf"
			} else {
				append intf_list "$intf , "
			}
		}
	} else {
		# the list does not contain any valid entries
		action_syslog priority 2 msg "Environment variable im_intf does not define any valid interfaces. Check syntax."
		exit 0
	}
}

##### Create lists containing CLI cmds #####

# define a list containing the commands to enable island mode
# these are the commands to be run from global config mode
lappend cmds_enable "interface range $intf_list"
lappend cmds_enable " switchport access vlan 904"
lappend cmds_enable " switchport mode access"
lappend cmds_enable " no switchport private-vlan host-association 201 401"
lappend cmds_enable " no ip access-group in"
lappend cmds_enable " no shutdown"

# define a list containing the commands to disable island mode
# these are the commands to be run from global config mode
lappend cmds_disable "interface range $intf_list"
lappend cmds_disable " switchport private-vlan host-association 201 401"
lappend cmds_disable " switchport mode private-vlan host"
lappend cmds_disable " no switchport access vlan 904"
lappend cmds_disable " ip access-group ACL_SERVICE_TECH_IN in"
lappend cmds_enable " no shutdown"

##### Create procedures for how to enable and disable Island Mode #####

# define the proc to handle enabling Island Mode on an interface
proc im_enable {} {

  global cmds_enable
  global intf_list

  # open a cli for cmd input
  if [catch {cli_open} result] {
      error $result $errorInfo
  } else {
      array set cli1 $result
  }

  # enter exec mode to allow for the show commands
  if [catch {
    cli_exec $cli1(fd) "enable"
    cli_exec $cli1(fd) "conf t"
    cli_exec $cli1(fd) "interface range $intf_list"
    cli_exec $cli1(fd) "shutdown"
    after 15000
    foreach command $cmds_enable {
      cli_exec $cli1(fd) "$command"
    }
    cli_exec $cli1(fd) "end"
  } _cli_result] {
    error $_cli_result $errorInfo
  }

  # close connection
  catch {cli_close $cli1(fd) $cli1(tty_id)} result
}

# define the proc to handle disabling Island Mode on an interface
proc im_disable {} {

  global cmds_disable
  global intf_list

  # open a cli for cmd input
  if [catch {cli_open} result] {
      error $result $errorInfo
  } else {
      array set cli1 $result
  }

  # execute commands
  if [catch {
    cli_exec $cli1(fd) "enable"
    cli_exec $cli1(fd) "conf t"
    cli_exec $cli1(fd) "interface range $intf_list"
    cli_exec $cli1(fd) "no shutdown"
    after 15000
    foreach command $cmds_disable {
      cli_exec $cli1(fd) "$command"
    }
    cli_exec $cli1(fd) "end"
  } _cli_result] {
    error $_cli_result $errorInfo
  }

  # close connection
  catch {cli_close $cli1(fd) $cli1(tty_id)} result
}

##### Handle the various events that the script can be called #####

# put event details into an array
array set event_details [event_reqinfo_multi]

# check which event was triggered and perform the appropriate actions
if { [info exists event_details(up)] } {
  im_disable
  set status 0
} elseif { [info exists event_details(down)] } {
  im_enable
  set status 1
} elseif { [info exists event_details(reboot)] } {
  im_enable
  set status 2
}

# check to see if the status variable is defined
if { [info exists status] } {
  # status == 0 indicates island mode should be disabled
  # status == 1 indicates island mode should be enabled
  # status == 2 indicates island mode should be enabled after a bootup of the device
  if { $status == 0 } {
    # verify island mode is not in effect
    action_syslog priority 5 msg "Island Mode is disabled"
  } elseif { $status == 1 } {
    # verify island mode is in effect
    action_syslog priority 5 msg "Island Mode is enabled"
  } elseif { $status == 2 } {
    # verify island mode is in effect after startup
    action_syslog priority 5 msg "Island Mode is enabled after startup"
  }
}

#
# end of script
#

exit 0
