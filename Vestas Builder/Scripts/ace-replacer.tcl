::cisco::eem::event_register_none sync yes maxrun 300 queue_priority low nice 0
::cisco::eem::description "This script will handle terminal input and replace ACL entries according to provided inputs"

########################################
#
# Script name: ace-replacer.tcl
# Version: 1.0.1
#
# Alias in running-config:
#   alias exec ace-replacer event manager run ace-replacer.tcl
#
# This script is to be run manually on
# the device using the command:
#   tclsh <scriptlocation> "arg1"
#
# First argument is the 127 host address
# to look for in ACL entries.
#
# The script supports single or multiple lines as replacement values.
# These values must be entered one line at a time
# and is finished with a blank line.
#
# Eg.
# Router1#ace-replacer 127.0.100.1
# Enter replacement entries and finish with a blank line
# 192.168.1.1
# 192,168.2.2
# 192.168.100.0 0.0.0.255
# 
# Processing input...
# Done!
# Router1#
#
########################################

namespace import ::cisco::eem::*
namespace import ::cisco::lib::*

proc enable { input_pattern replace } {
	# pattern and replacement values are valid
	# continue with finding matches in ACLs
	set acl_list [grab_acls $input_pattern]

	if { $acl_list == "" } {
		puts "ERROR: No ACLs with matching entries"
		exit 1
	}

	set clean_acl [acl_cleaner $acl_list]

	set acl_changed [acl_replacer $clean_acl $replace $input_pattern]

	acl_writer $acl_changed

	set acl_status [join [grab_acls $input_pattern]]

	if { $acl_status == "" } {
		puts "Success!"
	} else {
		puts "Failure!"
	}
}

proc disable { input_pattern replace } {
	# invert the search pattern
	set search_pattern [lindex $replace 0]
	# pattern and replacement values are valid
	# continue with finding matches in ACLs
	set acl_list [grab_acls $search_pattern]

	if { $acl_list == "" } {
		puts "ERROR: No ACLs with matching entries"
		exit 1
	}

	set clean_acl [acl_cleaner $acl_list]

	set acl_changed [acl_unreplacer $clean_acl $replace $input_pattern]

	acl_writer $acl_changed

	set acl_status [join [grab_acls $search_pattern]]

	if { $acl_status == "" } {
		puts "Success!"
	} else {
		puts "Failure!"
	}
}

proc verify_address { input } {
	set status 0
	foreach i $input {
		# single regex pattern to match on both host address or subnet and wildcard combination
		set match_result [regexp {^((?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(?:\.(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3})(?: |$)|((?:0|1|3|7|15|31|63|127|255)(?:\.(?:0|1|3|7|15|31|63|127|255)){3}$)} $i input_all host wildcard]

		# if there is a match then process the results further
		if {$match_result == 1} {
			if { [regexp {^(?:0|1|3|7|15|31|63|127|255)(?:\.(?:0|1|3|7|15|31|63|127|255)){3}$} $wildcard] } {
				# valid host and wildcard mask
				set status 1
			} else {
				# valid host address
				set status 2
			}
		} else {
			# not match result on first regex pattern - invalid input
			set status 0
		}
	}	
	return $status
}

proc grab_acls { input } {
	# open a cli for cmd input
	if [catch {cli_open} result] {
	    error $result $errorInfo
	} else {
	    array set cli1 $result
	}

	# enter exec mode to allow for the show commands
	if [catch {cli_exec $cli1(fd) "enable"} _cli_result] {
		error $_cli_result $errorInfo
	}

	if [catch {cli_exec $cli1(fd) "term length 0"} _cli_result] {
		error $_cli_result $errorInfo
	}

	# show the ACLs in the running-config and incl only ACL names and line matching the input pattern
	if [catch {cli_exec $cli1(fd) "show run | incl ^ip access-list extended .*|^ .* $input"} _cli_result] {
		error $_cli_result $errorInfo
	} else {
		set output_raw $_cli_result
	}

	# split the output into a list
	# one item per newline
	set output_lines [split $output_raw \n]

	# run through the list and extract all the ACL names that has entries matching the input pattern
	set new_acl 0
	set acl_names ""
	foreach line $output_lines {
		if { [regexp {^ip access-list extended (.*)$} $line match_all match_name] } {
			# this line indicates the beginning of a new ACL
			set new_acl 1
			set acl_name $match_name
		} elseif { $new_acl == 1 && [regexp {^ (?:permit|deny|remark)} $line]} {
			# there is a line matching the pattern in the currently selected ACL
			# add the ACL name to the list
			lappend acl_namelist [string trimright $acl_name]
			# reset variables
			unset acl_name
			set new_acl 0
		} else {
			# not a new ACL name and current ACL is already on the list
			set new_acl 0
		}
	}

	# grab all ACLs from the list and return it
	if { [info exist acl_namelist] } {
		foreach name $acl_namelist {
			# show the ACL matching the name in the current list entry
			set cmd "show run | section ^ip access-list extended $name$"
			if [catch {cli_exec $cli1(fd) $cmd} _cli_result] {
				error $_cli_result $errorInfo
			} else {
				# add the ACL to the acl_list
				lappend acl_list $_cli_result
			}
			after 1000
		}
	} else {
		set acl_list {}
	}

	# Close open cli before exit.
	after 4000
	catch {cli_close $cli1(fd) $cli1(tty_id)} result

	# return the list of ACLs that match the pattern
	return $acl_list
}

proc acl_cleaner { acl_list } {
	foreach acl $acl_list {
		set acl_lines [split $acl \n]
		foreach acl_line $acl_lines {
			if { [regexp {(?:ip access-list extended)|(?: permit|deny|remark)} $acl_line] } {
				lappend clean_acl [string trimright $acl_line]
			}
		}
	}
	set return_acl $clean_acl
	return $return_acl
}

proc acl_replacer { acl replace_list pattern } {
	# split the input up by line
	foreach acl_line $acl {
		if { [regexp " (?:permit|deny) .* (?:host $pattern)" $acl_line] } {
			# the line needs changing
			foreach repl $replace_list {
				if { [regexp {^(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(?:\.(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}$} $repl] } {
					set repl_line "host $repl"
				} else {
					set repl_line $repl
				}
				regsub -all "host $pattern" $acl_line "$repl_line" new_line
				lappend new_acl $new_line
				unset new_line
			}
		} else {
			# there is no pattern match in this line so just add it to the list as is
			if { [regexp {ip access-list extended .*} $acl_line] } {
				lappend new_acl "no $acl_line"
				lappend new_acl $acl_line
			} elseif { [regexp { (?:permit|deny|remark) .*} $acl_line] } {
				lappend new_acl $acl_line
			}
		}
	}

	#set replacement_acl [join $new_acl]
	set replacement_acl $new_acl
	
	if { ![info exist replacement_acl] } {
		set replacement_acl {}
	}

	return $replacement_acl
}

proc acl_unreplacer { acls_list pattern_list replace } {
	set prev_replaced ""	
	foreach acl_line $acls_list {
		set pattern_match 0
		foreach pattern $pattern_list {
			if { [regexp " (?:permit|deny) .* (?:$pattern)" $acl_line] } {
				# the line needs changing
				set pattern_match 1
				if { [regexp {^(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(?:\.(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}$} $pattern] } {
					regsub -all "host $pattern" $acl_line "host $replace" new_line
				} else {
					regsub -all "$pattern" $acl_line "host $replace" new_line
				}

				if { $new_line == $prev_replaced } {
					# the line is the same as already committed - skip it
				} else {
					# the line is new - commit it to the list and add it to the prev_replaced variable for next pass of the loop
					lappend new_acl $new_line 
					set prev_replaced $new_line
				}
			}
		}
		# there is no pattern match in this line so just add it to the list as is
		if { [regexp {^ip access-list .*} $acl_line] } {
			lappend new_acl "no $acl_line"
			lappend new_acl $acl_line
		} elseif { [regexp {^ (?:permit|deny|remark) .*} $acl_line] && $pattern_match == 0} {
			lappend new_acl $acl_line
		}
	}
	#lappend replacement_acl [join $new_acl]
	set replacement_acl $new_acl

	if { ![info exist replacement_acl] } {
		set replacement_acl {}
	}

	return $replacement_acl
}

proc acl_writer { replacement_acl } {
	# open a cli for cmd input
	if [catch {cli_open} result] {
	    error $result $errorInfo
	} else {
	    array set cli1 $result
	}

	# enter exec mode to allow for the show commands
	if [catch {cli_exec $cli1(fd) "enable"} _cli_result] {
		error $_cli_result $errorInfo
	}

	# enter configuration mode to allow for the config commands
	if [catch {cli_exec $cli1(fd) "conf t"} _cli_result] {
		error $_cli_result $errorInfo
	}

	# replace ACLs
	foreach line $replacement_acl {
		if [catch {cli_exec $cli1(fd) "$line"} _cli_result] {
			error $_cli_result $errorInfo
		}
		after 10
	}

	# exit out of configuration mode
	if [catch {cli_exec $cli1(fd) "end"} _cli_result] {
		error $_cli_result $errorInfo
	}

	# Close open cli before exit.
	catch {cli_close $cli1(fd) $cli1(tty_id)} result
}


#### beginning of script

# get arguments from user
array set arr_einfo [event_reqinfo]
set argc $arr_einfo(argc)

# verify the arg input
if { $arr_einfo(argc) == 1 } {
	set input_pattern $arr_einfo(arg1)
} elseif { $arr_einfo(argc) == 2 } {
	if { $arr_einfo(arg1) == "-invert" } {
		set input_invert 1
		set input_pattern $arr_einfo(arg2)
	} else {
		puts "ERROR: unknown input given"
		exit 1
	}
} else {
	puts "ERROR: invalid amount of inputs given"
	exit 1
}

# check whether to run in normal or inverted mode
if { ![info exist input_invert] } {
	set input_invert 0
}

# verify the input pattern supplied by the user
set verified [verify_address $input_pattern]
if { $verified == 2 } {
	# valid pattern is detected
} else { 
	puts "ERROR: Invalid pattern detected"
	exit 1
}
unset verified

# if the arg is valid
# continue to gather replacement info from user input

puts "Enter replacement entries and finish with a blank line"

set loop 0
while { $loop == 0 } {
	gets stdin newline
	if { $newline != "" } {
		lappend replace $newline
		unset newline
	} else {
		# the line is blank - stop processing further inputs
		set loop 1
	}
}
unset loop

if { ![info exists replace] } {
	puts "ERROR: no replacement value given"
	exit 1
}

puts "Processing input..."

# verify the replacement values supplied by the user
set verified [verify_address $replace]
if { $verified == 1 || $verified == 2 } {
	# valid replacement value or values are detected
} else { 
	puts "ERROR: Invalid replacement value detected"
	exit 1
}
unset verified

# run either enable or disable proc based on -invert arg given by user
if { $input_invert == 0 } {
	enable $input_pattern $replace
} elseif { $input_invert == 1 } {
	disable $input_pattern $replace
}

puts "Done!"