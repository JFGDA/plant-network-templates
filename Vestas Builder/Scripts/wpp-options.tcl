::cisco::eem::event_register_none sync yes maxrun 120 queue_priority low nice 0
::cisco::eem::description "This script will handle terminal input and add/delete/change an option entry in a pre-defined environment variable"

########################################
#
# Script name: wpp-options.tcl
# Version: 1.1.0
#
# Alias in running-config:
#   alias exec wpp-options event manager run wpp-options.tcl
#
# This script is to be run manually on
# the device using the command:
#   tclsh <scriptlocation> "arg1" "arg2"
#
# First argument is the name of the 
# environment variable to work with.
#
# Second argument is the line that
# should be added or removed from
# the environment variable.
#
# Environment variables supported:
# - options_global
# - options_regional
#
# Supports single-arguments as listed below:
# - help
#		shows the help section for reference
# - list
#		shows the currently configured options in the variable strings
#
########################################

namespace import ::cisco::eem::*
namespace import ::cisco::lib::*

# get_options grabs the information from the environment variables
# based on a basename with incremental digits appended
# returns a list once it reacheas an increment that does not exist
# NOTE: does not support discontiguous numbering
proc get_options { base_name } {
	# set a list of globals that match the base_name pattern
	set global_options_list [lsort [get_globals $base_name]]

	# set the globals to global accessible for proc
	foreach {var} $global_options_list {
		global $var
	}

	# check whether the global_options_list contains anyhing
	if { $global_options_list != "" } {
		# loop through the global options environment variables to list the individual options contained
		foreach {env_var} $global_options_list {
			# split the options in the env variable by the semi-colon delimiter
			set list [split [set $env_var] ";"]
			# loop through the contents of the newly created list
			foreach opt_item $list {
				# append each option to the options_list
				lappend options_list $opt_item
			}
		}
	}

	# return the resulting options_list when finished iterating over the contents of the environment variables
	if { [info exists options_list] } {
		return $options_list
	} else {
		# return empty if no match exists in the globals
		return ""
	}
}

proc get_globals { pattern } {
	# set the pattern to the supplied pattern plus any characters succeeding it
	set pattern "${pattern}*"
	# get all globals that match the pattern
	set global_vars [info globals $pattern]
	# split the global_vars variable into a list using space as a delimiter
	set global_list [split $global_vars " "]
	
	# return the list
	return $global_list
}

proc write_options { env_var_base options_list_in } {

	set max_len 200
	set list_len 0
	set lists_req 1
	set count 0
	set last_item 0
	set last_item [llength $options_list_in]
	set current_env_var "options_${env_var_base}${lists_req}"

	# open a cli for cmd input
	if [catch {cli_open} result] {
	    error $result $errorInfo
	} else {
	    array set cli1 $result
	}
	
	# add the updated options list to the running-config environment variable
	if [catch {cli_exec $cli1(fd) "enable"} _cli_result] {
		error $_cli_result $errorInfo
		return 0
	}
	if [catch {cli_exec $cli1(fd) "conf t"} _cli_result] {
		error $_cli_result $errorInfo
		return 0
	}

	foreach item $options_list_in {
		set item_len [string length $item]
		set commit 0
		set split_list 0

		if { [expr $list_len + $item_len] <= $max_len } {
			set list_len [expr $list_len + $item_len]
			lappend options_list_out $item
		} else {
			incr lists_req
			set split_list 1
			set commit 1
		}

		incr count
		if { $count >= $last_item } {
			set last_item 1
			set commit 1
		}

		if { $commit } {
			# commit the list
			set options_list_out [join $options_list_out ";"]
			if [catch {cli_exec $cli1(fd) "event manager environment $current_env_var $options_list_out"} _cli_result] {
				error $_cli_result $errorInfo
				return 0
			}
			# flush the list
			unset options_list_out
			set list_len 0
		}

		# set the current_env_var before next commit
		set current_env_var "options_${env_var_base}${lists_req}"

		# add the split item to the next output list if required
		if { $split_list != 0 } {
			set list_len [expr $list_len + $item_len]
			lappend options_list_out $item

			if { $last_item != 0 } {
				# commit the list now as it is the final item
				set options_list_out [join $options_list_out ";"]
				if [catch {cli_exec $cli1(fd) "event manager environment $current_env_var $options_list_out"} _cli_result] {
					error $_cli_result $errorInfo
					return 0
				}
			}
		}
	}

	# Close open cli before exit.
	if [catch {cli_exec $cli1(fd) "end"} _cli_result] {
	    error $_cli_result $errorInfo
	}
	catch {cli_close $cli1(fd) $cli1(tty_id)} result

	return 1
}

array set arr_einfo [event_reqinfo]
set argc $arr_einfo(argc)

# handle single-argument options
if { $arr_einfo(argc) <= 1 } {
	if { $arr_einfo(argc) == 1 } {
		set choice $arr_einfo(arg1)
	} else {
		set choice "-help"
	}

	# handle the help option
	if { [ regexp -nocase {\-help} $choice ] } {
		puts "This script is intended for adding/changing/deleting options from the running-config environment variable."
		puts "Command syntax is: wpp-options [global|regional] <option to add/change/remove>"
		puts "It will automatically detect whether to add, change or remove an option based on the input."
		puts "  - It does not exist with the specified option name = add option to the list"
		puts "  - It exists with the specified option name, but versions differ = change to new version"
		puts "  - It exists with the specified option name and exact version = remove option from the list"
		puts ""
		puts "Example:"
		puts "wpp-options global CMS_v1.1"
		puts ""
		puts "Also supports single-argument options for other tasks as listed below"
		puts "Help option: wpp-options -help"
		puts "List options: wpp-options -list"
		puts ""
	} elseif { [ regexp -nocase {\-list} $choice ] } {
		# handle the list option
		puts "Listing current options enabled on device"

		set options_list [get_options "options_global"]
		if { $options_list != "" } {
			puts "Global Options:"
			foreach opt $options_list {
				puts " -  $opt"
			}
			puts ""
		} else {
			puts "No Global Options installed."
		}

		set options_list [get_options "options_regional"]
		if { $options_list != "" } {
			puts "Regional Options:"
			foreach opt $options_list {
				puts " -  $opt"
			}
			puts ""
		} else {
			puts "No Regional Options installed."
		}
	}

} else {
	if { $arr_einfo(argc) != 2 } {
	    puts "ERROR: This script requires exactly two inputs to run."
	    puts "Please try again."
	} else {
		# set variable information
		set env_var $arr_einfo(arg1)
		set option_item $arr_einfo(arg2)

		# check whether the env_var selection is for a global or regional option
		if { [ regexp -nocase {^global$|^regional$} $env_var ] } {
			# the env_var is valid

			# ensure the env_var is configured as lower case
			set env_var [string tolower $env_var]

			# set the options_var and check_version variables - used to verify inputs later
			set options_var ""
			set check_version "no"

			# set the base environment variable name based on the $env_var input
			if { $env_var == "global" } {
				set base_name "options_global"
			} elseif { $env_var == "regional" } {
				set base_name "options_regional"
			}

			# check if the option input is properly formatted and create input variables for later use
			set input_result [regexp {^(.*)_v([0-9]+[\.][0-9]+)$} $option_item input_all input_opt input_vers]

			if { $input_result >= 1 && ![regexp {;} $option_item]} {
				# check if the option exists in the environment variable
				#puts "Checking if '$option_item' is already on the $env_var list"
				# iterate over the cur_options list
				# get the currently installed options from config
				set current_options_list [ get_options $base_name ]
				set position 0
				foreach opt $current_options_list {
					set match_result [regexp "($input_opt)_v(\[0-9\]+\[\.\]\[0-9\]+)" $opt match_all match_opt match_vers]
					if { $match_result >= 1 } {
						# the option is present - check the version
						set check_version "yes"
						set target_pos $position
					}
					incr position
				}
				unset position

				if { $check_version == "yes" } {
					# the list contains the option - version must be checked
					if { $input_vers == $match_vers } {
						# the version is a match so delete the item
						puts "Deleting option"
						set all_options [lreplace $current_options_list $target_pos $target_pos ]
						#set all_options [join $all_options ";"]

					} else {
						# the version does not match - overwrite existing item
						puts "Changing option"
						set all_options [lreplace $current_options_list $target_pos $target_pos $option_item]
						#set all_options [join $all_options ";"]
					}
				} else {
					# the option is not on the list - ergo it must be a new addition
					puts "Adding option"
					set all_options $current_options_list
					lappend all_options $option_item
					#set all_options [join $all_options ";"]
				}

				if { [write_options $env_var $all_options] } {
					puts "Done"
				} else {
					puts "ERROR: An error occurred while saving options to device configuration"
				}

			} else {
				puts "ERROR: Invalid input detected ($option_item)"
			}

		} else {
			puts "ERROR: Invalid selection for options environment variable"
		}
	}
}