::cisco::eem::event_register_timer cron name crontimer2 cron_entry $Poll_Time maxrun 240
#----------------------------------
# EEM policy that will periodically check for expired crypto keys and send SNMP trap
#
# Copyright (c) February 2009, jepalmer@cisco.com
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of Cisco, the name of the copyright holder nor the
#    names of their respective contributors may be used to endorse or
#    promote products derived from this software without specific prior
#    written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
#
#----------------------------------
### The following EEM environment variables are used:
###
### Poll_Time (mandatory)               - A CRON specification that determines
###                                       when the policy will run. See the
###                                       IOS Embedded Event Manager
###                                       documentation for more information
###                                       on how to specify a cron entry.
###
###     Example: event manager environment Poll_Time 0-59/1 0-23/1 * * 0-7
###
###
### Days_to_Warn (optional)     - Specifies number of days to start warning
###
###     Example: event manager environment Days_to_Warn 7
###
###
### Debug_File (optional)               - A filename to append the output to.
###                                       If this variable is defined, the
###                                       output is appended to the specified
###                                       file with a timestamp added.
###
###     Example: event manager environment Debug_File disk0:/my_file.log
###
# Script name: tm_ccen.tcl
# Version: 1.0.0
#----------------------------------
# check if all the env variables we need exist

# If any of them doesn't exist, print out an error msg and quit
if {![info exists Days_to_Warn]} {
        set days 60
        } else {
        set days $Days_to_Warn
        }
set seconds [expr $days*24*60*60]               ;# Calculate seconds from days
set show_crypto_cmd {show crypto ca certificates}

namespace import ::cisco::eem::*
namespace import ::cisco::lib::*

set routername [info hostname]
# 1. execute the command
if [catch {cli_open} result] {
    error $result $errorInfo
} else {
    array set cli1 $result
}
if [catch {cli_exec $cli1(fd) "en"} result] {
    error $result $errorInfo
}
# save exact execution time for command
set time_now [clock seconds]
# execute command
if [catch {cli_exec $cli1(fd) $show_crypto_cmd} result] {
    error $result $errorInfo
} else {
    set cmd_output $result
    # format output: remove trailing router prompt
    set prompt [format "(.*\n)(%s)(\\(config\[^\n\]*\\))?(#|>)" $routername]
    if [regexp "[set prompt]" $result dummy cmd_output] {
       # do nothing, match will be in $cmd_output
    } else {
       # did not match router prompt so use original output
       set cmd_output $result
    }
}
if [catch {cli_close $cli1(fd) $cli1(tty_id)} result] {
    error $result $errorInfo
}

# 2. Scans "show crypto ca certificates" looking for expiring and expired licenses
    set pat {end[ \t]+date:[ \t]+([0-9]{1,2}[-:/][0-9]{1,2}[-:/][0-9]{1,2}[ \t]+[A-Z]{3}[ \t]+[ADFJMNOS][a-z]{2}[ \t]+[0-9]{1,2}[ \t]+[0-9]{1,4})}
    set license_conter 0
    set expired_conter 0
    foreach line [split $cmd_output \n ] {
        regexp $pat $line _match clock_data
        if {[info exists clock_data]} {
                set cv [clock scan $clock_data]
                set clock_target [expr ([clock seconds]+$seconds>=$cv) ? 1 : 0]
                set clock_expired [expr ([clock seconds]>=$cv) ? 1 : 0]
                if { $clock_target==1 } {
                        if { $clock_expired==0 } {
                                set strdata [format "crypto ca certificate expires on %s on %s" $routername $clock_data]
                                set intdata1 0
                                incr license_conter
                        } else {
                                set strdata [format "crypto ca certificate expired on %s on %s" $routername $clock_data]
                                set intdata1 1
                                incr expired_conter
                        }
                        action_snmp_trap intdata1 $intdata1 strdata $strdata
                        if {$_cerrno != 0} {
                                set result [format "component=%s; subsys err=%s; posix err=%s;\n%s" \
                                $_cerr_sub_num $_cerr_sub_err $_cerr_posix_err $_cerr_str]
                                error $result
                        }
                        action_syslog priority crit msg $strdata
                        if {$_cerrno != 0} {
                                set result [format "component=%s; subsys err=%s; posix err=%s;\n%s" \
                                $_cerr_sub_num $_cerr_sub_err $_cerr_posix_err $_cerr_str]
                                error $result
                        }

                }
                unset clock_data
                }
        }

# 3. if Debug_File is defined, then attach it to the file
if {[info exists Debug_File]} {
    # attach output to file
    if [catch {open $Debug_File w+} result] {
        error $result
    }
    set fileD $result
    # save timestamp of command execution
    #      (Format = 00:53:44 PDT Mon May 02 2005)
    set time_now [clock format $time_now -format "%T %Z %a %b %d %Y"]
    puts $fileD "%%% Timestamp = $time_now %%%"
    puts $fileD $cmd_output
    puts $fileD "$license_conter license(s) expiring within $days days"
    puts $fileD "$expired_conter license(s) expired"
    close $fileD
}
