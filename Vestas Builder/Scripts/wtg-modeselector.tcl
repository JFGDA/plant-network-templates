::cisco::eem::description "Enables or disables man-in-turbine mode on the switch"
::cisco::eem::event_register_syslog tag normal pattern "%PLATFORM_ENV-1-EXTERNAL_ALARM_CONTACT_CLEAR:" nice 0 queue_priority high maxrun 60
::cisco::eem::event_register_syslog tag isolation pattern "%PLATFORM_ENV-1-EXTERNAL_ALARM_CONTACT_ASSERT:"
::cisco::eem::event_register_timer tag reboot cron cron_entry @reboot
::cisco::eem::trigger {
  ::cisco::eem::correlate event normal or event isolation or event reboot
}

##########################################################################
#
# Script name: wtg-modeselector.tcl
# Version: 1.0.2
#
# Enables or disables the modeselector mode parameters on a switch.
# On reboot it will detect what the alarm contact is currently set to
# and perform the appropriate actions.
#
# Prerequisites:
#  - The following vlan access-maps should already be
#    defined in the configuration.
#     vlan access-map VAM_WTG_MC
#     vlan access-map VAM_WTG_DEBUG
#
#
#
##########################################################################

namespace import ::cisco::eem::*
namespace import ::cisco::lib::*

# define a list containing the isolation mode commands
# these are the commands to be run from global config mode
lappend cmds_isolation "vlan access-map VAM_WTG_MC 30"
lappend cmds_isolation " match ip address ACL_WTG_MC_ISOLATED_ALLOWED"
lappend cmds_isolation " action forward"
lappend cmds_isolation "vlan access-map VAM_WTG_MC 40"
lappend cmds_isolation " match ip address ACL_WTG_MC_ISOLATED_DENIED"
lappend cmds_isolation " action drop"
lappend cmds_isolation "vlan access-map VAM_WTG_DEBUG 30"
lappend cmds_isolation " match ip address ACL_WTG_DEBUG_ISOLATED_ALLOWED"
lappend cmds_isolation " action forward"
lappend cmds_isolation "vlan access-map VAM_WTG_DEBUG 40"
lappend cmds_isolation " match ip address ACL_WTG_DEBUG_ISOLATED_DENIED"
lappend cmds_isolation " action drop"

# define a list containing the normal mode commands
# these are the commands to be run from global config mode
lappend cmds_normal "no vlan access-map VAM_WTG_MC 30"
lappend cmds_normal "no vlan access-map VAM_WTG_MC 40"
lappend cmds_normal "no vlan access-map VAM_WTG_DEBUG 30"
lappend cmds_normal "no vlan access-map VAM_WTG_DEBUG 40"

# define the proc to handle enabling Isolation Mode on a switch
proc isolation {} {

  global cmds_isolation

  # open a cli for cmd input
  if [catch {cli_open} result] {
      error $result $errorInfo
  } else {
      array set cli1 $result
  }

  # enter exec mode to allow for the show commands
  if [catch {
    cli_exec $cli1(fd) "enable"
    cli_exec $cli1(fd) "conf t"
    foreach command $cmds_isolation {
    	cli_exec $cli1(fd) "$command"
    }
    cli_exec $cli1(fd) "end"
  } _cli_result] {
    error $_cli_result $errorInfo
  }

  # close connection
  catch {cli_close $cli1(fd) $cli1(tty_id)} result
}

# define the proc to handle disabling Isolation Mode on a switch
proc normal {} {

  global cmds_normal

  # open a cli for cmd input
  if [catch {cli_open} result] {
      error $result $errorInfo
  } else {
      array set cli1 $result
  }

  # execute commands
  if [catch {
    cli_exec $cli1(fd) "enable"
    cli_exec $cli1(fd) "conf t"
    foreach command $cmds_normal {
    	cli_exec $cli1(fd) "$command"
    }
    cli_exec $cli1(fd) "end"
  } _cli_result] {
    error $_cli_result $errorInfo
  }

  # close connection
  catch {cli_close $cli1(fd) $cli1(tty_id)} result
}

# define a proc to handle verification of current mode on the switch
proc verify_assert {} {

  # open a cli for cmd input
  if [catch {cli_open} result] {
      error $result $errorInfo
  } else {
      array set cli1 $result
  }

  # execute commands
  if [catch {
    cli_exec $cli1(fd) "show env alarm-contact | sec CONTACT 1"
  } _cli_result] {
    error $_cli_result $errorInfo
  }

  # do a regexp on the output of the previous command to get only the status
  set cur_status [regexp -all -inline -line {not asserted|asserted} $_cli_result]

  # close connection
  catch {cli_close $cli1(fd) $cli1(tty_id)} result

  # look at the current assertion status of the alarm contact setting
  if { [regexp {^asserted$|^not asserted$} $cur_status] } {
    # status is valid so the mode should be asserted or not asserted
    set r_value $cur_status
  } else {
    # an error has occured
    action_syslog priority 5 msg "An unknown error has occured during alarm assert verification."
    exit 1
  }

  return $r_value
}



# put event details into an array to call later
array set event_details [event_reqinfo_multi]

# check which event was triggered and call the appropriate proc
if { [info exists event_details(isolation)] } {
  isolation
  set status 1
} elseif { [info exists event_details(normal)] } {
  normal
  set status 0
} elseif { [info exists event_details(reboot)] } {
  set reboot_status [verify_assert]
  if { $reboot_status == "asserted" } {
    isolation
    set status 1
  } elseif { $reboot_status == "not asserted" } {
    normal
    set status 0
  }
}

# check to see if the status variable is defined
if { [info exists status] } {
	# status == 1 indicates isolation mode should be in effect
	# status == 0 indicates normal mode should be in effect
	if { $status == 1 } {
		# verify isolation mode is in effect
		action_syslog priority 5 msg "Isolation Mode is in effect"
	} elseif { $status == 0 } {
		# verify normal mode is in effect
		action_syslog priority 5 msg "Normal Mode is in effect"
	}
} else {
  action_syslog priority 5 msg "No status is available. The modeselector script failed and performed no actions."
}

#
# end of script
#

exit 0
