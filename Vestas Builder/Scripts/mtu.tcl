::cisco::eem::event_register_timer watchdog time 86400 maxrun 300 queue_priority low nice 0

########################################
#
# Script name: mtu.tcl
# Version: 1.1.1
#
# Script for testing Path MTU to HQ over the Internet
# The script is built to run once a day using the watchdog timer
# It works by sending ping packets of various sizes with the DF-bit set
#
# The script is unable to detect if the MTU needs to be set higher
# it can ONLY detect if the MTU needs to be set lower
#
########################################

namespace import ::cisco::eem::*
namespace import ::cisco::lib::*

# check for environment variables being configured
# the script requires the mtu_ip environment variable

if {![info exists mtu_ip]} {
  action_syslog priority 4 msg "Environment variable missing (mtu_ip)"
  exit 1
}
if {![info exists wan_intf]} {
  action_syslog priority 4 msg "Environment variable missing (wan_intf)"
  exit 1
}

# set the initial tracker value (must be a standard binary value - 1024, 512, 256 etc.)
set tracker 1024

# open a cli for cmd input
if [catch {cli_open} result] {
    error $result $errorInfo
} else {
    array set cli1 $result
}

# enter exec mode to allow for the show commands
if [catch {cli_exec $cli1(fd) "enable"} _cli_result] {
	error $_cli_result $errorInfo
}

# do a regular ping to the intended destination to make sure it works without the DF-bit or size changes
if [catch {cli_exec $cli1(fd) "ping $mtu_ip"} _cli_result] {
	error $_cli_result $errorInfo
}

# check the result of the prevoius regular ping to make sure it was a success
# if not, send a syslog warning and exit the program
if { [regexp {(!!!)} $_cli_result ] } {

	# Set the MTU variable equal to the configured interface IP MTU
	if [catch {cli_exec $cli1(fd) "show ip int $wan_intf | incl MTU"} _cli_result] {
		error $_cli_result $errorInfo
	}

	set _regexp_result [regexp {([\d]+)} $_cli_result match mtu]
	set configured_mtu $mtu

} else {
	action_syslog priority 3 msg "The address $mtu_ip did not respond to a regular ping or was unreliable in its response - MTU calculation cannot be performed at this time."
	exit 1
}

# ping the same destination with the configured MTU and the DF-bit set
if [catch {cli_exec $cli1(fd) "ping $mtu_ip df-bit size $configured_mtu repeat 1"} _cli_result] {
	error $_cli_result $errorInfo
}

if { [regexp {(!)} $_cli_result ] } {
  # The ping goes through with the maximum MTU
  # no need to run the rest of the script
  exit 1
} else {
  # The configured MTU does not receive a reply with the DF-bit set
  # MTU detection is needed

  # Make sure the seed tracker fits with the configured size of the MTU
  while { $tracker > $mtu } {
    set tracker [expr {$tracker / 2}]
  }

  while { $tracker > 0 } {

  	# exit the script if the MTU value falls below the threshold of 36 bytes
  	if { $mtu < 36 } {
  		action_syslog priority 3 msg "MTU got lower than threshold allows (36 bytes)"
  		exit 1
  	}

  	# send a ping with the DF-bit set using the current MTU value
  	if [catch {cli_exec $cli1(fd) "ping $mtu_ip df-bit size $mtu repeat 1"} _cli_result] {
  		error $_cli_result $errorInfo
  	}

  	# based on the result of the previous ping the script will do one of the following
  	# if the previous MTU setting resulted in an echo-reply then add the current tracker value to the MTU
  	# if it failed, the tracker value is subtracted from the current MTU
  	if { [regexp {(!)} $_cli_result ] } {
  		set mtu [expr {$mtu + $tracker}]
  	} else {
  		set mtu [expr {$mtu - $tracker}]
  	}

  	# halve the value of the tracker
  	set tracker [expr {$tracker / 2}]

  	# enter into the final stretch of calculation when the tracker is equal to 1
  	# this sends out three pings: one with no change, one with 1 added and one with 1 subtracted
  	# this is to find the exact MTU value even
  	if { $tracker == 1 } {
  		# send a ping without modifying the current MTU value (no-change)
  		if [catch {cli_exec $cli1(fd) "ping $mtu_ip df-bit size $mtu repeat 1"} _cli_result_nochg] {
  			error $_cli_result_nochg $errorInfo
  		}

  		# subtract 1 from the current MTU value and send a ping (minus)
  		set mtu_minus [expr {$mtu - 1}]
  		if [catch {cli_exec $cli1(fd) "ping $mtu_ip df-bit size $mtu_minus repeat 1"} _cli_result_minus] {
  			error $_cli_result_minus $errorInfo
  		}

  		# add 1 to the current MTU value and send a ping (plus)
  		set mtu_plus [expr {$mtu + 1}]
  		if [catch {cli_exec $cli1(fd) "ping $mtu_ip df-bit size $mtu_plus repeat 1"} _cli_result_plus] {
  			error $_cli_result_plus $errorInfo
  		}

  		# do the final trimming based on the outcome of the previous 3 ping attempts
  		if { [regexp {(!)} $_cli_result_nochg ] } {
  			if { [regexp {(!)} $_cli_result_plus ] } {
  				set mtu $mtu_plus
  			}
  		} else {
  			if { [regexp {(!)} $_cli_result_minus ] } {
  				set mtu $mtu_minus
  			} else {
  				set mtu [expr {$mtu_minus - 1}]
  			}
  		}

  		# set the tracker to 0 to exit out of the while loop and continue with the script
  		set tracker 0
  	}
  }

  # look at the MTU values and send a syslog with level warning if the configured and measured MTU do not match
  if { $configured_mtu != $mtu } {
  	action_syslog priority 4 msg "MTU mismatch detected: configured on $wan_intf is $configured_mtu and the measured MTU is $mtu"
  }

}

# Close open cli before exit.
catch {cli_close $cli1(fd) $cli1(tty_id)} result

exit 0