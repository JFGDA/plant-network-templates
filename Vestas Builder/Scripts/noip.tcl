::cisco::eem::event_register_timer tag wdog watchdog time 43200 maxrun 300 queue_priority low nice 0
::cisco::eem::event_register_timer tag reboot cron cron_entry @reboot
::cisco::eem::event_register_none tag usr_exec
::cisco::eem::description "Dynamic DNS updating for NO-IP provider"
::cisco::eem::trigger {
  ::cisco::eem::correlate event wdog or event reboot or event usr_exec
}

########################################
#
# Script name: noip.tcl
# Version: 1.1.6
#
# The watchdog timer is set for every 12 hours to run the script. It also runs at boot after 30 seconds of wait time.
# It checks to see if the current IP address on Gi0/0/0 is different compared to the environment variable last_addr
# To force the script to run just replace the environment variable last_addr content with something else (or nothing at all)
#
# The script also supports being behind an upstream NAT router.
# This, however, forces the router to send an HTTP request to the DDNS provider every time it runs (12 hrs).
# If directly connected, the HTTP request will only be sent in case the IP address on the interface is different from the last_addr environment variable
#
########################################

namespace import ::cisco::eem::*
namespace import ::cisco::lib::*

# define a procedure to handle user debugs
proc usr_debug { msg } {
	array set event_details [event_reqinfo_multi]
	if { [info exists event_details(usr_exec)] } {
		puts "$msg"
	}
}

# check for environment variables being configured
# the script requires the topdomain, last_addr and noip_pass environment variable

if {![info exists topdomain]} {
  # if the env variable is not set, assume the default of globalwpp.net
  set topdomain globalwpp.net
}
if {![info exists last_addr]} {
  action_syslog priority 4 msg "Environment variable missing (last_addr)"
  exit 1
}
if {![info exists noip_pass]} {
  action_syslog priority 4 msg "Environment variable missing (noip_pass)"
  exit 1
}
if {![info exists wan_intf]} {
  action_syslog priority 4 msg "Environment variable missing (wan_intf)"
  exit 1
}

# put event details into an array
array set event_details [event_reqinfo_multi]

set override_hostname 0

if {![info exists noip_hostname_override]} {
  action_syslog priority 4 msg "Environment variable missing (noip_hostname_override)"
} else {
  # check the noip_hostname_variable if it exists
  # it will indicate a 'no' if the site numbers are to be derived from the noip_hostname_override
  # otherwise it will indicate the hostname to use instead.
  if { $noip_hostname_override != "no" } {
    $override_hostname = 1
  }
}

if { $override_hostname == 0 } {
  # do not override the hostname
  # take the router's hostname and truncate it to the SP number
  set hname [info hostname]
  set _regexp_result [regexp {([0-9]+)} $hname -> sitenumbers]

  # make a syslog entry if the hostname is incorrectly formatted (the regexp fails)
  if { $_regexp_result == 0 } {
      action_syslog priority 4 msg "The hostname is incorrect"
      exit 1
  }
} else {
  # override the hostname with the name in the environment variable noip_hostname_override
  set sitenumbers $noip_hostname_override
}

# set username variable for the DDNS provider
set user "$sitenumbers%23heknuddns"

# set the fully qualified domain name (FQDN) that is used by the provider to identify the host
# this is based on the truncated hostname as the site and the topdomain environment variable
set fqdn "$sitenumbers.$topdomain"

# open a cli for cmd input
if [catch {cli_open} result] {
    error $result $errorInfo
} else {
    array set cli1 $result
}

# if running after a boot wait 30 seconds before continuing
if { [info exists event_details(reboot)] } {
	# clear the last_addr environment variable to ensure a request is sent after a fresh boot of the device
	action_syslog priority 6 msg "Clearing last_addr environment variable upon reboot."
	if [catch {cli_exec $cli1(fd) "enable"} _cli_result] {
		error $_cli_result $errorInfo
		exit 1
	}
	if [catch {cli_exec $cli1(fd) "conf t"} _cli_result] {
		error $_cli_result $errorInfo
		exit 1
	}
	if [catch {cli_exec $cli1(fd) "event manager environment last_addr"} _cli_result] {
		error $_cli_result $errorInfo
		exit 1
	}

	# wait 60 seconds before continuing
	after 60000
}

# get the current IP address on the WAN interface - try 3 times before putting an error
set get_addr_count 1
set got_address 0
while { $get_addr_count <= 3 } {
	if [catch { cli_exec $cli1(fd) "show int $wan_intf | i Internet address is" } _cli_result] {
	    error $_cli_result $errorInfo
	}

	# do a regexp on the output of the previous command to get only the IP address
	# this is done using the match parameter and putting it into the addr variable
	set _regexp_result [regexp {Internet address is ([\d\.]+)} $_cli_result match addr]

	# check the regex result and wait for 5 seconds if the regex failed
	if { $_regexp_result == 0 } {
		usr_debug "No IP address found on interface $wan_intf. Waiting..."
	    after 5000
	} else {
		usr_debug "The interface $wan_intf has an ip address of $addr"
		set got_address 1
		set get_addr_count 100
	}

	set get_addr_count [expr {$get_addr_count + 1}]
	usr_debug "Incrementing loop counter to $get_addr_count"
}

if { $got_address != 1 } {
	action_syslog priority 5 msg "Interface $wan_intf does not have an IP address"
	exit 1
}

# check to see if the current IP address on the interface is different from the last_addr environment variable
usr_debug "Comparing current address ($addr) with last registered address ($last_addr)"
if { $addr != $last_addr } {
	# the addresses do not match and the script will send an HTTP request to the DDNS provider and process the response

	# set the update url and encode the user/passwd to be ready for transmission
	set url  "http://dynupdate.no-ip.com/nic/update?hostname=$fqdn"
	set auth "Basic [base64::encode $user:$noip_pass]"
	set headerl [list Authorization $auth]

	# send an HTTP request to the dynamic DNS provider
	if {[catch {http::geturl $url -headers $headerl -queryblocksize 50 -type "text/plain" } token]} {
		# send a syslog message if the request fails and exit
		action_syslog priority 4 msg "HTTP request failed"
		exit 1
	}

	# trim the returned HTTP data to the noip_resp variable
	set noip_resp [string trim [http::data $token]]

	usr_debug "Response from NO-IP = $noip_resp"

	# do a regexp on the response from the provider to match out the response code
	set _regexp_result [regexp {([A-Za-z]+)} $noip_resp match noip_code]

	if { $_regexp_result == 0 } {
		action_syslog priority 4 msg "No code could be derived from the HTTP response (HTTP response: $noip_resp)"
		exit 1
	} else {
		# check to see if the return code was nohost
		if { [string equal $noip_code nohost] } {
			# a return code of "nohost" means that the hostname was wrong
			# send a syslog error message containing the FQDN and the HTTP response for troubleshooting
			action_syslog priority 4 msg "Wrong hostname $fqdn"
			exit 1
		} else {
			# do a regexp on the response from the provider to match out the IP address returned (if any)
			set _regexp_result [regexp {([\d\.]+)} $noip_resp match global_addr]

			if { $_regexp_result == 0 } {
				# no IP address was detected in the response from the DDNS provider
				# and the response was not "nohost"
				# assume an error has occurred
				action_syslog priority 4 msg "No address could be derived from the response (HTTP response: $noip_resp)"
				exit 1
			} else {
				# an IP address was present in the response from the DDNS provider

				# do a check to see if the interface address is the same as the global address
				if { $addr == $global_addr } {
					# the addresses match and so the router must be directly connected to the Internet
					usr_debug "The router is directly connected to the Internet"

					# set the last_addr environment variable to the interface IP address
					if [catch {cli_exec $cli1(fd) "enable"} _cli_result] {
						error $_cli_result $errorInfo
						exit 1
					}
					if [catch {cli_exec $cli1(fd) "conf t"} _cli_result] {
						error $_cli_result $errorInfo
						exit 1
					}
					if [catch {cli_exec $cli1(fd) "event manager environment last_addr $addr"} _cli_result] {
						error $_cli_result $errorInfo
						exit 1
					}
					usr_debug "Environment variable 'last_addr' has been updated to $addr"

				} else {
					# the addresses do not match and so we must assume that the device is behind an upstream NAT router
					usr_debug "The router is presumably connected to the Internet through an upstream NAT'ed service"

					# clear the last_addr environment variable
					if [catch {cli_exec $cli1(fd) "enable"} _cli_result] {
						error $_cli_result $errorInfo
						exit 1
					}
					if [catch {cli_exec $cli1(fd) "conf t"} _cli_result] {
						error $_cli_result $errorInfo
						exit 1
					}
					if [catch {cli_exec $cli1(fd) "event manager environment last_addr"} _cli_result] {
						error $_cli_result $errorInfo
						exit 1
					}
					usr_debug "Environment variable 'last_addr' has been cleared."
				}
			}
		}
	}
} else {
	usr_debug "Both variables are equal. Skipping sending a NO-IP request..."
}

# Close open cli before exit.
if [catch {cli_exec $cli1(fd) "end"} _cli_result] {
    error $_cli_result $errorInfo
}
catch {cli_close $cli1(fd) $cli1(tty_id)} result

usr_debug "Script ended properly."

exit 0
