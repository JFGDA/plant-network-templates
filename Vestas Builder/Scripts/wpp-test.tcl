::cisco::eem::event_register_none maxrun 600 queue_priority low nice 0
::cisco::eem::description "This script is built to automate a lot of the testing done when installing or troubleshooting a WPP site"

########################################
#
# Script name: wpp-test.tcl
# Version: 1.1.3
#
# this script will not output any logs or snmp traps
# this is strictly for use by an installation or support technician
# the output will be sent to the terminal window
# the script will not change any settings automatically,
# but it will inform the user of any misconfigurations or issues uncovered
# any issues found by the script should be verified manually before implementing af solution
#
########################################

namespace import ::cisco::eem::*
namespace import ::cisco::lib::*

########################################
#
# Check Internet connectivity by
# 1.) ping default gateway
# 2.) ping 8.8.8.8
# 3.) and ping a DNS address (google.com)
# 4.) stability test w/ lots of pings
#
########################################
proc test_conn {} {

  # open a cli for cmd input
  if [catch {cli_open} result] {
      error $result $errorInfo
  } else {
      array set cli1 $result
  }

  # enter exec mode to allow for the show commands
  if [catch {cli_exec $cli1(fd) "enable"} _cli_result] {
    error $_cli_result $errorInfo
  }

  puts "-------------------- Connectivity Test --------------------"
  puts ""

  puts -nonewline "Pinging Default Gateway... "
  flush stdout

  set ping_def_gw_status 0

  # get the default gateway IP and ping it to check for reliability
  #
  # show the contents of the routing table and include only the line stating the default gw
  if [catch {cli_exec $cli1(fd) "show ip route | incl Gateway of"} _cli_result] {
    puts "ERROR EXEC CMD (failed to get IP RIB information)"
    # the CLI exec command failed
  } else {
    # do a regexp on the output of the previous commands to get only the IP address
    # this is done using the match parameter and putting it into the default_gw variable
    set _regexp_result [ regexp {Gateway of last resort is ([\d\.]+)} $_cli_result match default_gw ]

    if { $_regexp_result == 0 } {
      puts "ERROR REGEXP (Regexp failed to process the RIB information)"
      # no default gateway address could be matched from the CLI output
    } else {
      if [catch { cli_exec $cli1(fd) "ping $default_gw timeout 1 repeat 20" } _cli_result] {
        puts "ERROR EXEC CMD (failed to ping default gateway)"
        # the CLI exec command failed
      } else {
        # get the percentage of successful pings from the previous cli cmd
        set _regexp_result [ regexp {Success rate is ([\d]+) percent} $_cli_result match ping_pct ]

        if { $_regexp_result == 0 } {
          puts "ERROR REGEXP (Regexp failed to process the PING output)"
          # regexp could not process the results of the ping command correctly
        } else {
          if { $ping_pct > 85 } {
            puts "OK"
            set ping_def_gw_status 1
          } else {
            puts "FAILED"
            puts "Successful pings: $ping_pct%"
          }
        }
      }
    }
  }

  puts ""
  puts -nonewline "Pinging Internet IP address... "
  flush stdout

  set ping_internet_ip_status 0

  if [catch { cli_exec $cli1(fd) "ping 8.8.8.8 repeat 10" } _cli_result] {
    puts "ERROR EXEC CMD (failed to ping default gateway)"
    # the CLI exec command failed
  } else {
    set _regexp_result [ regexp {Success rate is ([\d]+) percent} $_cli_result match ping_pct ]
    if { $_regexp_result == 0 } {
      puts "ERROR REGEXP (Regexp failed to process the PING output)"
      # regexp could not process the results of the ping command correctly
    } else {
      if { $ping_pct > 85 } {
        puts "OK"
        set ping_internet_ip_status 1
      } else {
        puts "FAILED"
        puts "Successful pings: $ping_pct%"
      }
    }
  }

  puts ""
  puts -nonewline "Testing DNS lookup... "
  flush stdout

  set dns_lookup_status 0

  if [catch { cli_exec $cli1(fd) "ping google.com repeat 1" } _cli_result] {
    puts "ERROR EXEC CMD (failed to ping DNS address)"
    # the CLI exec command failed
  } else {
    set _regexp_result [ regexp {ICMP Echos to ([\d\.]+)} $_cli_result match dns_ip_resolv ]
    if { $_regexp_result == 0 } {
      puts "ERROR DNS (DNS name could not be resolved)"
      # regexp could not process the results of the ping command correctly
    } else {
      puts "OK"
      set dns_lookup_status 1
    }
  }

  # do reliability testing to the default gateway (preferred)
  # but only if the gateway responds to pings
  #
  puts ""
  set reliable_status 0

  if { $ping_def_gw_status == 1 } {
    puts -nonewline "Testing reliability towards Default Gateway... "
    flush stdout

    if [catch { cli_exec $cli1(fd) "ping $default_gw timeout 1 repeat 1000" } _cli_result] {
      puts "ERROR EXEC CMD (failed to ping default gateway)"
      # the CLI exec command failed
    } else {
      # get the percentage of successful pings from the previous cli cmd
      set _regexp_result [ regexp {Success rate is ([\d]+) percent} $_cli_result match ping_pct ]

      if { $_regexp_result == 0 } {
        puts "ERROR REGEXP (Regexp failed to process the PING output)"
        # regexp could not process the results of the ping command correctly
      } else {
        if { $ping_pct > 85 } {
          puts "OK"
          set reliable_status 1
        } else {
          puts "FAILED"
          puts "Successful pings: $ping_pct%"
        }
      }
    }
  } else {
    # fallback to testing reliability towards an Internet destination
    # but only if the Internet address responds to pings
    #
    if {$ping_internet_ip_status == 1} {
      puts -nonewline "Testing reliability towards Internet destination... "
      flush stdout

      if [catch { cli_exec $cli1(fd) "ping 8.8.8.8 timeout 1 repeat 1000" } _cli_result] {
        puts "ERROR EXEC CMD (failed to ping default gateway)"
        # the CLI exec command failed
      } else {
        # get the percentage of successful pings from the previous cli cmd
        set _regexp_result [ regexp {Success rate is ([\d]+) percent} $_cli_result match ping_pct ]

        if { $_regexp_result == 0 } {
          puts "ERROR REGEXP (Regexp failed to process the PING output)"
          # regexp could not process the results of the ping command correctly
        } else {
          if { $ping_pct > 85 } {
            puts "OK"
            set reliable_status 1
          } else {
            puts "FAILED"
            puts "Successful pings: $ping_pct%"
          }
        }
      }
    } else {
      puts "Skipping reliability tests (no reliable host to test towards)"
    }
  }

  puts "----------------------------------------"

  # close connection
  catch {cli_close $cli1(fd) $cli1(tty_id)} result

}

########################################
#
# update DDNS record and verify the
# global routable IP address
#
########################################

proc test_ddns {} {

  # open a cli for cmd input
  if [catch {cli_open} result] {
      error $result $errorInfo
  } else {
      array set cli1 $result
  }

  # enter exec mode to allow for the show commands
  if [catch {cli_exec $cli1(fd) "enable"} _cli_result] {
    error $_cli_result $errorInfo
  }

  global topdomain
  global noip_pass
  global wan_intf
  global noip_hostname_override

  puts "----------------------------------------"
  puts -nonewline "Testing DDNS service... "
  flush stdout

  if {![info exists noip_hostname_override]} {
    action_syslog priority 4 msg "Environment variable missing (noip_hostname_override)"
    set override_hostname 0
  } else {
    # check the noip_hostname_variable if it exists
    # it will indicate a 'no' if the site numbers are to be derived from the noip_hostname_override
    # otherwise it will indicate the hostname to use instead.
    if { $noip_hostname_override != "no" } {
      set override_hostname 1
    } else {
      set override_hostname 0
    }
  }

  if { $override_hostname == 0 } {
    # do not override the hostname
    # take the router's hostname and truncate it to the SP number
    set hname [info hostname]
    set _regexp_result [regexp {([0-9]+)} $hname -> sitenumbers]

    # make a syslog entry if the hostname is incorrectly formatted (the regexp fails)
    if { $_regexp_result == 0 } {
        action_syslog priority 4 msg "The hostname is incorrect"
        exit 1
    }
  } else {
    # override the hostname with the name in the environment variable noip_hostname_override
    set sitenumbers $noip_hostname_override
  }

  # set username variable for the DDNS provider
  set user "$sitenumbers%23heknuddns"

  # check for the existence of the environment variable topdomain
  if {![info exists topdomain]} {
    puts " ERROR ENV VAR (topdomain environment variable not set)"
    # environment variable topdomain not found
  } else {
    # set the fully qualified domain name (FQDN) that is used by the provider to identify the host
    # this is based on the truncated hostname as the site and the topdomain environment variable
    set fqdn "$sitenumbers.$topdomain"
    # set the update url and encode the user/passwd to be ready for transmission
  	set url  "http://dynupdate.no-ip.com/nic/update?hostname=$fqdn"
  	set auth "Basic [base64::encode $user:$noip_pass]"
  	set headerl [list Authorization $auth]

  	# send an HTTP request to the dynamic DNS provider
  	if {[catch {http::geturl $url -headers $headerl -queryblocksize 50 -type "text/plain" } token]} {
  		puts "ERROR GETURL (Failed to get proper URL response)"
      # HTTP request failed
  	} else {
      # trim the returned HTTP data to the noip_resp variable
    	set noip_resp [string trim [http::data $token]]

    	# do a regexp on the response from the provider to match out the response code
    	set _regexp_result [regexp {([A-Za-z]+)} $noip_resp match noip_code]

      if { $_regexp_result == 0 } {
        puts "ERROR REGEXP (Regexp failed to process the response)"
        # No code could be derived from the HTTP response
    	} else {
        # check to see if the return code was badauth
        if { [string equal $noip_code badauth] } {
          puts "ERROR BADAUTH ($noip_code)"
          # authentication failed to the DDNS service provider
        } else {
          # check to see if the return code was nohost
      		if { [string equal $noip_code nohost] } {
            puts "ERROR NOHOST ($noip_code)"
      			# a return code of "nohost" means that the hostname was wrong
      		} else {
            if { [string equal $noip_code good] || [string equal $noip_code nochg] } {
              # the return code was 'good' or 'nochg' meaning that all is OK
              puts "OK"
            } else {
              # an unknown error has occurred
              # the return code was none of these
              # good, nochg or nohost
              puts "ERROR UNKNOWN (Return code: $noip_code)"
            }
          }
        }
      }
    }
  }

  puts -nonewline "Hostname override: "
  flush stdout

  if {$override_hostname == 0 } {
    puts "disabled"
  } else {
    puts "enabled"
    puts "FQDN: $fqdn"
  }

  puts "----------------------------------------"

  ########################################
  #
  # Check NAT status
  #
  ########################################

  puts "----------------------------------------"
  puts -nonewline "Testing NAT status... "
  flush stdout

  if {[info exists noip_resp]} {

    # do a regexp on the response from the provider to match out the global IP address
    set _regexp_result [regexp {([\d\.]+)} $noip_resp match noip_global]

    if { $_regexp_result == 0 } {
      puts "ERROR REGEXP (Regexp failed to process the response)"
      # No address could be derived from the HTTP response done in the previous section (DDNS check)
    } else {
      # get the current IP address on the WAN interface
      if [catch {cli_exec $cli1(fd) "show int $wan_intf | incl Internet address is"} _cli_result] {
        puts "ERROR EXEC CMD (failed to get interface IP information)"
        # the CLI exec command failed
      } else {
        # do a regexp on the output of the previous commands to get only the IP addresses
        # this is done using the match parameter and putting it into the addr variable
        regexp {Internet address is ([\d\.]+)} $_cli_result match noip_intf_addr

        puts "OK"
        puts ""

        if { $noip_intf_addr == $noip_global } {
          puts "NAT Status: not behind NAT"
        } else {
          puts "NAT Status: behind NAT"
        }

        puts "Global IP address: $noip_global"
        puts "Local IP address: $noip_intf_addr"
      }
    }
  } else {
    puts "ERROR (no web response)"
  }

  puts "----------------------------------------"

  # close connection
  catch {cli_close $cli1(fd) $cli1(tty_id)} result
}

########################################
#
# check to see if the MTU is configured
# correctly on the WAN interface
#
########################################

proc test_mtu {} {

  # open a cli for cmd input
  if [catch {cli_open} result] {
      error $result $errorInfo
  } else {
      array set cli1 $result
  }

  # enter exec mode to allow for the show commands
  if [catch {cli_exec $cli1(fd) "enable"} _cli_result] {
    error $_cli_result $errorInfo
  }

  global mtu_ip
  global wan_intf

  puts "----------------------------------------"
  puts -nonewline "Testing MTU configuration... "
  flush stdout

  # set the initial tracker value (must be a standard binary value - 1024, 512, 256 etc.)
  set tracker 1024

  if {![info exists mtu_ip]} {
    puts "ERROR ENV VAR (mtu_ip environment variable not set)"
    # environment variable mtu_ip not found
  } else {
    # do a regular ping to the intended destination to make sure it works without the DF-bit or size changes
    if [catch {cli_exec $cli1(fd) "ping $mtu_ip"} _cli_result] {
    	error $_cli_result $errorInfo
    }

    # check the result of the prevoius regular ping to make sure it was a success
    # if not, send a syslog warning and exit the program
    if { ![regexp {(!!!)} $_cli_result ] } {
      puts "ERROR PING (Destination cannot be pinged reliably)"
      # The address $mtu_ip did not respond to a regular ping or was unreliable in its response - MTU calculation cannot be performed at this time.
    } else {
      # Set the MTU variable equal to the configured interface IP MTU
      if [catch {cli_exec $cli1(fd) "show ip int $wan_intf | incl MTU"} _cli_result] {
        error $_cli_result $errorInfo
      }
      set _regexp_result [regexp {([\d]+)} $_cli_result match mtu]
      set configured_mtu $mtu

      # Make sure the seed tracker fits with the configured size of the MTU
      while { $tracker > $mtu } {
        set tracker [expr {$tracker / 2}]
      }

      # enter a while loop for discovering the path MTU
      while { $tracker > 0 } {

        # exit the script if the MTU value falls below the threshold of 36 bytes
        if { $mtu < 36 } {
          puts "ERROR MTU LOW (The MTU was decremented lower than the set threshold)"
          # MTU got lower than threshold allows (36 bytes)
          set tracker 0
        }

        # send a ping with the DF-bit set using the current MTU value
        if [catch {cli_exec $cli1(fd) "ping $mtu_ip df-bit size $mtu repeat 1"} _cli_result] {
          error $_cli_result $errorInfo
        }

        # based on the result of the previous ping the script will do one of the following
        # if the previous size setting resulted in an echo-reply
        # then add the current tracker value to the MTU
        # if it failed, the tracker value is subtracted from the current MTU
        if { [regexp {(!)} $_cli_result ] } {
          set mtu [expr {$mtu + $tracker}]
        } else {

          # Make sure the tracker does not decrement below 0
          while { $tracker > $mtu } {
            set tracker [expr {$tracker / 2}]
          }
          set mtu [expr {$mtu - $tracker}]
        }

        # halve the value of the tracker
        set tracker [expr {$tracker / 2}]

        # enter into the final stretch of calculation when the tracker is equal to 1
        # this sends out three pings: one with no change, one with 1 added and one with 1 subtracted
        # this is to find the exact MTU value even
        if { $tracker == 1 } {
          # send a ping without modifying the current MTU value (no-change)
          if [catch {cli_exec $cli1(fd) "ping $mtu_ip df-bit size $mtu repeat 1"} _cli_result_nochg] {
            error $_cli_result_nochg $errorInfo
          }

          if { [regexp {(!)} $_cli_result_nochg ] } {
            # add 1 to the current MTU value and send a ping (plus)
            set mtu_plus [expr {$mtu + 1}]
            if [catch {cli_exec $cli1(fd) "ping $mtu_ip df-bit size $mtu_plus repeat 1"} _cli_result_plus] {
              error $_cli_result_plus $errorInfo
            }
            if { [regexp {(!)} $_cli_result_plus ] } {
              set mtu $mtu_plus
            } else {
              # MTU value is already correct
            }
          } else {
            # subtract 1 from the current MTU value and send a ping (minus)
            set mtu_minus [expr {$mtu - 1}]
            if [catch {cli_exec $cli1(fd) "ping $mtu_ip df-bit size $mtu_minus repeat 1"} _cli_result_minus] {
              error $_cli_result_minus $errorInfo
            }
            if { [regexp {(!)} $_cli_result_minus ] } {
              set mtu $mtu_minus
            } else {
              set mtu [expr {$mtu_minus - 1}]
            }
          }
          # set the tracker to 0 to exit out of the while loop and continue with the script
          set tracker 0
        }
      }
    }

    if {[info exists configured_mtu]} {
      # look at the MTU values and output to screen
      if { $configured_mtu != $mtu } {
        puts "ERROR MTU MISMATCH (The MTU needs to be configured lower)"
        puts "$wan_intf MTU = $configured_mtu"
        puts "Measured MTU = $mtu"
        # MTU mismatch detected: configured on WAN interface is $configured_mtu and the measured MTU is $mtu
      } else {
        puts "OK"
    }
    } else {
      # an error has occurred previously and the detection cannot continue.
    }
  }

  puts "----------------------------------------"

  # close connection
  catch {cli_close $cli1(fd) $cli1(tty_id)} result

}

########################################
#
# check the DMVPN status on the router
# there should be two DMVPN hubs
# and they should be in the UP state
#
########################################

proc test_dmvpn {} {

  # open a cli for cmd input
  if [catch {cli_open} result] {
      error $result $errorInfo
  } else {
      array set cli1 $result
  }

  # enter exec mode to allow for the show commands
  if [catch {cli_exec $cli1(fd) "enable"} _cli_result] {
    error $_cli_result $errorInfo
  }

  puts "----------------------------------------"
  puts -nonewline "Checking DMVPN connection status... "
  flush stdout

  if [catch {cli_exec $cli1(fd) "show dmvpn interface tunnel10 | count UP"} _cli_result] {
    error $_cli_result $errorInfo
  }
  regexp {([\d]+)} $_cli_result match dmvpn_hub_up

  if { $dmvpn_hub_up == 0 } {
    puts "There is no connection to any of the DMVPN hubs!"
  }
  if { $dmvpn_hub_up == 1 } {
    puts "There is only 1 connection to a DMVPN hub!"
  }
  if { $dmvpn_hub_up > 1 } {
    puts "There are redundant connections to DMVPN hubs."
  }

  puts "----------------------------------------"

  # close connection
  catch {cli_close $cli1(fd) $cli1(tty_id)} result

}

########################################
#
# give basic instructions as to the use
# of this script
#
########################################

puts "This script will allow for quick verifcation of various functions including"
puts "- DDNS registration and verification"
puts "- Interface MTU verification"
puts "- NAT status and more..."
puts " "
puts "For best results, issue the command 'terminal length 0' before running this script."

########################################
#
# set the variables that determine
# which tests to run
#
########################################

set run_conn 0
set run_ddns 0
set run_mtu 0
set run_dmvpn 0

# ask user for input
# for running connectivity tests
while { $run_conn < 1 } {
  puts "Run connectivity tests?"
  puts "('yes' or 'no')"
  gets stdin answ_conn

  if { [regexp {^[Yy]{1}[Ee]{0,1}[Ss]{0,1}$} $answ_conn] } {
    set run_conn 1
  } elseif { [regexp {^[Nn]{1}[Oo]{0,1}$} $answ_conn] } {
    set run_conn 2
  }  else {
    puts "Invalid input. Try again."
    puts ""
  }
}

# ask user for input
# for running DDNS tests
while { $run_ddns < 1 } {
  puts "Run DDNS and NAT tests?"
  puts "('yes' or 'no')"
  gets stdin answ_ddns

  if { [regexp {^[Yy]{1}[Ee]{0,1}[Ss]{0,1}$} $answ_ddns] } {
    set run_ddns 1
  } elseif { [regexp {^[Nn]{1}[Oo]{0,1}$} $answ_ddns] } {
    set run_ddns 2
  }  else {
    puts "Invalid input. Try again."
    puts ""
  }
}

# ask user for input
# for running MTU tests
while { $run_mtu < 1 } {
  puts "Run MTU tests?"
  puts "('yes' or 'no')"
  gets stdin answ_mtu

  if { [regexp {^[Yy]{1}[Ee]{0,1}[Ss]{0,1}$} $answ_mtu] } {
    set run_mtu 1
  } elseif { [regexp {^[Nn]{1}[Oo]{0,1}$} $answ_mtu] } {
    set run_mtu 2
  }  else {
    puts "Invalid input. Try again."
    puts ""
  }
}

# ask user for input
# for running DMVPN tests
while { $run_dmvpn < 1 } {
  puts "Run DMVPN tests?"
  puts "('yes' or 'no')"
  gets stdin answ_dmvpn

  if { [regexp {^[Yy]{1}[Ee]{0,1}[Ss]{0,1}$} $answ_dmvpn] } {
    set run_dmvpn 1
  } elseif { [regexp {^[Nn]{1}[Oo]{0,1}$} $answ_dmvpn] } {
    set run_dmvpn 2
  }  else {
    puts "Invalid input. Try again."
    puts ""
  }
}

# call the procs the user selected
if { $run_conn == 1} {
  test_conn
} else {
  puts "Bypassing connectivity tests"
}
if { $run_ddns == 1} {
  test_ddns
} else {
  puts "Bypassing DDNS tests"
}
if { $run_mtu == 1} {
  test_mtu
} else {
  puts "Bypassing MTU tests"
}
if { $run_dmvpn == 1} {
  test_dmvpn
} else {
  puts "Bypassing DMVPN tests"
}

puts ""
puts "NOTE: the preceding information has not been saved anywhere"
puts ""
puts "End of Tests"

#
# end of script
#

exit 0
