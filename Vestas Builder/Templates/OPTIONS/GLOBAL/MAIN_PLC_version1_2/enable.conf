!### OPTION: MAIN_PLC
!### VERSION: {{VER_NUM}}
!### ENABLE
!### templates 2.1.2 and above


!### VOB begin
!### MAIN1 begin
!
conf t
interface GigabitEthernet1/10
 switchport access vlan 58
 switchport mode access
 switchport nonegotiate
 switchport block unicast
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
 no shutdown
!
interface Vlan58
 description MAIN-PLC
 ip flow monitor FLOWS_IN input
 ip address 10.56.135.253 255.255.255.0
 ip access-group MAIN_PLC_IN in
 no ip redirects
 no ip proxy-arp
 ip verify unicast source reachable-via rx allow-self-ping
 standby version 2
 standby 0 ip 10.56.135.254
 standby 0 priority 110
 standby 0 preempt
 standby 0 authentication md5 key-chain HSRP-KEYS
 bfd interval 600 min_rx 600 multiplier 3
 no shutdown
!
vlan 58
 no shutdown
 exit
!
ip access-list extended MAIN_PLC_IN
 remark ### ACL SECTION Park-Service Begin
 remark Park-Service-NETWORK-CONTROL
 permit udp any host 224.0.0.102
 remark Park-Service-NTP
 permit udp any host 10.56.140.123 eq ntp
 permit udp any host 10.56.139.123 eq ntp
 remark Park-Service-LOG
 permit udp any 10.56.131.0 0.0.0.255 eq syslog
 permit tcp any 10.56.131.0 0.0.0.255 eq cmd
 remark Park-Service-DNS
 permit udp any host 10.56.139.1 eq domain
 permit tcp any host 10.56.139.1 eq domain
 remark Park-Service-TROUBLESHOOT
 permit icmp any any echo-reply
 permit icmp any any time-exceeded
 permit icmp any any unreachable
 permit icmp any any ttl-exceeded
 permit udp any any range 33434 33564
 permit icmp any any echo
 remark ### ACL SECTION Park-Service End
 remark ### ACL SECTION MAIN_PLC_RETURNTRAFFIC Begin
 permit tcp 10.56.135.0 0.0.0.255 any established
 permit udp 10.56.135.0 0.0.0.255 lt 1024 any gt 1023
 remark ### ACL SECTION MAIN_PLC_RETURNTRAFFIC End
 remark ### ACL SECTION MAIN_PLC Begin
 remark SWITCHGEAR
 permit tcp 10.56.135.0 0.0.0.255 host 127.0.100.8 eq 44818
 permit tcp 10.56.135.0 0.0.0.255 host 127.0.100.8 eq 2222
 permit tcp 10.56.135.0 0.0.0.255 host 127.0.100.8 eq 2404
 remark ### ACL SECTION MAIN_PLC End
 remark ### ACL SECTION MAIN_PLC Blocktraffic Begin
 permit ip 10.56.135.0 0.0.0.255 10.56.135.0 0.0.0.255
 remark WTG_DEBUG-PHASE2-EXCLUDE
 permit ip any 10.60.8.0 0.0.7.255
 remark WTG_LAB-PHASE2-EXCLUDE
 permit ip any 10.63.0.0 0.0.7.255
 remark AVIATION_LIGHT-PHASE2-EXCLUDE
 permit ip any host 127.0.100.1
 remark LVM-PHASE2-EXCLUDE
 permit ip any host 127.0.100.2
 remark VPA_SUP-PHASE2-EXCLUDE
 permit ip any 10.56.136.0 0.0.0.255
 remark VMET-PHASE2-EXCLUDE
 permit ip any host 127.0.100.3
 remark CURRENT-WTG-PHASE2-EXCLUDE
 permit ip any host 127.0.100.4
 deny   ip any 10.56.0.0 0.7.255.255
 permit ip any any
 remark ### ACL SECTION MAIN_PLC Blocktraffic End
!
end
!
! change the ACLs containing references to MAIN_PLC subnet
ace-replacer 127.0.100.10
10.56.135.0 0.0.0.255

!
wpp-options global MAIN_PLC_v{{VER_NUM}}
write memory

!
!### MAIN1 end
!


!
!### MAIN2 begin
!
conf t
!
interface GigabitEthernet1/10
 switchport access vlan 58
 switchport mode access
 switchport nonegotiate
 switchport block unicast
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
 no shutdown
!
interface Vlan58
 description MAIN-PLC
 ip flow monitor FLOWS_IN input
 ip address 10.56.135.252 255.255.255.0
 ip access-group MAIN_PLC_IN in
 no ip redirects
 no ip proxy-arp
 ip verify unicast source reachable-via rx allow-self-ping
 standby version 2
 standby 0 ip 10.56.135.254
 standby 0 preempt
 standby 0 authentication md5 key-chain HSRP-KEYS
 bfd interval 600 min_rx 600 multiplier 3
 no shutdown
!
vlan 58
 no shutdown
 exit
!
ip access-list extended MAIN_PLC_IN
 remark ### ACL SECTION Park-Service Begin
 remark Park-Service-NETWORK-CONTROL
 permit udp any host 224.0.0.102
 remark Park-Service-NTP
 permit udp any host 10.56.140.123 eq ntp
 permit udp any host 10.56.139.123 eq ntp
 remark Park-Service-LOG
 permit udp any 10.56.131.0 0.0.0.255 eq syslog
 permit tcp any 10.56.131.0 0.0.0.255 eq cmd
 remark Park-Service-DNS
 permit udp any host 10.56.139.1 eq domain
 permit tcp any host 10.56.139.1 eq domain
 remark Park-Service-TROUBLESHOOT
 permit icmp any any echo-reply
 permit icmp any any time-exceeded
 permit icmp any any unreachable
 permit icmp any any ttl-exceeded
 permit udp any any range 33434 33564
 permit icmp any any echo
 remark ### ACL SECTION Park-Service End
 remark ### ACL SECTION MAIN_PLC_RETURNTRAFFIC Begin
 permit tcp 10.56.135.0 0.0.0.255 any established
 permit udp 10.56.135.0 0.0.0.255 lt 1024 any gt 1023
 remark ### ACL SECTION MAIN_PLC_RETURNTRAFFIC End
 remark ### ACL SECTION MAIN_PLC Begin
 remark SWITCHGEAR
 permit tcp 10.56.135.0 0.0.0.255 host 127.0.100.8 eq 44818
 permit tcp 10.56.135.0 0.0.0.255 host 127.0.100.8 eq 2222
 permit tcp 10.56.135.0 0.0.0.255 host 127.0.100.8 eq 2404
 remark ### ACL SECTION MAIN_PLC End
 remark ### ACL SECTION MAIN_PLC Blocktraffic Begin
 permit ip 10.56.135.0 0.0.0.255 10.56.135.0 0.0.0.255
 remark WTG_DEBUG-PHASE2-EXCLUDE
 permit ip any 10.60.8.0 0.0.7.255
 remark WTG_LAB-PHASE2-EXCLUDE
 permit ip any 10.63.0.0 0.0.7.255
 remark AVIATION_LIGHT-PHASE2-EXCLUDE
 permit ip any host 127.0.100.1
 remark LVM-PHASE2-EXCLUDE
 permit ip any host 127.0.100.2
 remark VPA_SUP-PHASE2-EXCLUDE
 permit ip any 10.56.136.0 0.0.0.255
 remark VMET-PHASE2-EXCLUDE
 permit ip any host 127.0.100.3
 remark CURRENT-WTG-PHASE2-EXCLUDE
 permit ip any host 127.0.100.4
 deny   ip any 10.56.0.0 0.7.255.255
 permit ip any any
 remark ### ACL SECTION MAIN_PLC Blocktraffic End
!
end
!
! change the ACLs containing references to MAIN_PLC subnet
ace-replacer 127.0.100.10
10.56.135.0 0.0.0.255

!
wpp-options global MAIN_PLC_v{{VER_NUM}}
write memory

!
!### MAIN2 end
!### VOB end


!### VOC begin
!### MAIN1 begin
!
conf t
interface fastEthernet1/8
 switchport access vlan 58
 switchport mode access
 switchport nonegotiate
 switchport block unicast
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
 no shutdown
!
interface Vlan58
 description MAIN-PLC
 ip flow monitor FLOWS_IN input
 ip address 10.56.135.253 255.255.255.0
 ip access-group MAIN_PLC_IN in
 no ip redirects
 no ip proxy-arp
 ip verify unicast source reachable-via rx allow-self-ping
 standby version 2
 standby 0 ip 10.56.135.254
 standby 0 priority 110
 standby 0 preempt
 standby 0 authentication md5 key-chain HSRP-KEYS
 bfd interval 600 min_rx 600 multiplier 3
 no shutdown
!
vlan 58
 no shutdown
 exit
!
ip access-list extended MAIN_PLC_IN
 remark ### ACL SECTION Park-Service Begin
 remark Park-Service-NETWORK-CONTROL
 permit udp any host 224.0.0.102
 remark Park-Service-NTP
 permit udp any host 10.56.140.123 eq ntp
 permit udp any host 10.56.139.123 eq ntp
 remark Park-Service-LOG
 permit udp any 10.56.131.0 0.0.0.255 eq syslog
 permit tcp any 10.56.131.0 0.0.0.255 eq cmd
 remark Park-Service-DNS
 permit udp any host 10.56.139.1 eq domain
 permit tcp any host 10.56.139.1 eq domain
 remark Park-Service-TROUBLESHOOT
 permit icmp any any echo-reply
 permit icmp any any time-exceeded
 permit icmp any any unreachable
 permit icmp any any ttl-exceeded
 permit udp any any range 33434 33564
 permit icmp any any echo
 remark ### ACL SECTION Park-Service End
 remark ### ACL SECTION MAIN_PLC_RETURNTRAFFIC Begin
 permit tcp 10.56.135.0 0.0.0.255 any established
 permit udp 10.56.135.0 0.0.0.255 lt 1024 any gt 1023
 remark ### ACL SECTION MAIN_PLC_RETURNTRAFFIC End
 remark ### ACL SECTION MAIN_PLC Begin
 remark SWITCHGEAR
 permit tcp 10.56.135.0 0.0.0.255 host 127.0.100.8 eq 44818
 permit tcp 10.56.135.0 0.0.0.255 host 127.0.100.8 eq 2222
 permit tcp 10.56.135.0 0.0.0.255 host 127.0.100.8 eq 2404
 remark ### ACL SECTION MAIN_PLC End
 remark ### ACL SECTION MAIN_PLC Blocktraffic Begin
 permit ip 10.56.135.0 0.0.0.255 10.56.135.0 0.0.0.255
 remark WTG_DEBUG-PHASE2-EXCLUDE
 permit ip any 10.60.8.0 0.0.7.255
 remark WTG_LAB-PHASE2-EXCLUDE
 permit ip any 10.63.0.0 0.0.7.255
 remark AVIATION_LIGHT-PHASE2-EXCLUDE
 permit ip any host 127.0.100.1
 remark LVM-PHASE2-EXCLUDE
 permit ip any host 127.0.100.2
 remark VPA_SUP-PHASE2-EXCLUDE
 permit ip any 10.56.136.0 0.0.0.255
 remark VMET-PHASE2-EXCLUDE
 permit ip any host 127.0.100.3
 remark CURRENT-WTG-PHASE2-EXCLUDE
 permit ip any host 127.0.100.4
 deny   ip any 10.56.0.0 0.7.255.255
 permit ip any any
 remark ### ACL SECTION MAIN_PLC Blocktraffic End
!
end
!
! change the ACLs containing references to MAIN_PLC subnet
ace-replacer 127.0.100.10
10.56.135.0 0.0.0.255

!
wpp-options global MAIN_PLC_v{{VER_NUM}}
write memory
!
!### MAIN1 end
!
