version 15.2
no service pad
service timestamps debug datetime msec localtime show-timezone year
service timestamps log datetime msec localtime show-timezone year
service password-encryption
!
hostname {{PARK_NAME}}-WTG{{WTG_NUM}}-NAC-SW1
!
boot-start-marker
boot system flash:ie4000-universalk9-mz.152-7.E.bin
boot-end-marker
!
!
logging userinfo
logging buffered 32768
no logging console
no logging monitor
enable secret 5 $1$1fS9$Vbh62RgrW4AWBmwVeF1Cg0
!
username VestasOps secret 9 $9$3RyUQva/DHqHi5$DkluZAZ1BGXCVUZy0b4uj3oeGo8DrCeIOKKQdhKchnY
aaa new-model
!
!
aaa authentication login default group radius local
aaa authentication enable default group radius enable
aaa authorization console
aaa authorization exec default group radius local if-authenticated
!
!
!
!
!
!
aaa session-id common
system mtu routing 1500
!
!
!
ip domain-name localwpp.net
ip name-server 10.56.139.1
rep bpduleak
!
!
!
!
!
ptp mode e2etransparent
!
!
!
archive
 log config
  logging enable
  notify syslog contenttype plaintext
  hidekeys
 path scp://vestnet:g2TReP09@193.47.71.145/incoming/$h
 write-memory
 time-period 1440

!
!
spanning-tree mode mst
spanning-tree portfast edge default
spanning-tree portfast edge bpduguard default
spanning-tree extend system-id
!
spanning-tree mst configuration
 name wpp
 revision 13
 instance 1 vlan 7, 9, 951-1000
 instance 2 vlan 8, 10
 instance 3 vlan 2-6, 11-950, 1001-4094
!
errdisable recovery cause bpduguard
errdisable recovery cause psecure-violation
errdisable recovery cause storm-control
!
alarm profile defaultPort
 alarm not-operating
 syslog not-operating
 notifies not-operating
!
!

!
!
track 1 ip sla 1 reachability
 delay down 30 up 30
!
lldp run
!
!
!
!
!
interface GigabitEthernet1/1
 description To {{PARK_NAME}}-WTG{{WTG_NUM}}-TOW-SW1 Gi1/3
 switchport mode trunk
!
interface GigabitEthernet1/2
 description TTE-X18 P5
 switchport trunk allowed vlan 130
 switchport mode trunk
 switchport block unicast
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
 spanning-tree portfast edge trunk
!
interface GigabitEthernet1/3
 description SPARE
 switchport block unicast
 shutdown
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
!
interface GigabitEthernet1/4
 description SPARE
 switchport block unicast
 shutdown
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
!
interface FastEthernet1/5
 description SPARE
 switchport block unicast
 shutdown
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
!
interface FastEthernet1/6
 description SPARE
 switchport block unicast
 shutdown
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
!
interface FastEthernet1/7
 description T&V Downlink
 shutdown
!
interface FastEthernet1/8
 description CMS 4MW Option
 switchport private-vlan host-association 212 412
 switchport mode private-vlan host
 ip access-group ACL_CMS_IN in
 no cdp enable
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
 no shutdown
!
interface FastEthernet1/9
 description Aviation Light Option
 shutdown
!
interface FastEthernet1/10
 description SPARE
 switchport block unicast
 shutdown
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
!
interface FastEthernet1/11
 description Dedicated Islandmode Interface
 switchport access vlan 904
 switchport mode access
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
!
interface FastEthernet1/12
 description Service PC
 switchport private-vlan host-association 201 401
 switchport mode private-vlan host
 ip access-group ACL_SERVICE_TECH_IN in
 storm-control broadcast level 1.50 0.50
 storm-control multicast level 5.00 3.00
 storm-control action trap
!
interface Vlan1
 no ip address
 no ip route-cache
 shutdown
!
interface Vlan{{VL_ID}}
 ip address {{VL_ADDR}} 255.255.255.0
 no ip redirects
 no ip proxy-arp
 no shutdown
!
ip default-gateway {{VL_GATEWAY}}
ip forward-protocol nd
no ip http server
no ip http secure-server
!
ip ssh version 2
!
ip access-list standard ACL_SNMP_RO_ACCESS
 permit 10.56.128.0 0.0.0.255
 permit 10.56.129.0 0.0.0.255
 permit 10.56.178.0 0.0.0.255
 permit 10.56.134.0 0.0.0.255
 permit 10.57.1.0 0.0.0.255
ip access-list standard ACL_SSH_ALLOWED
 remark --- WAN1 SSH Source Intf ---
 permit 10.56.139.22
 remark --- Client VPN ---
 permit 10.126.254.0 0.0.0.31
 remark --- OSPF Link networks ---
 permit 10.56.0.0 0.0.0.15
 remark --- VOB Subnet ---
 permit 10.56.128.0 0.0.0.255
 remark --- Tool Subnet ---
 permit 10.56.129.0 0.0.0.255
 remark --- NRP Subnet ---
 permit 10.56.134.0 0.0.0.255
 remark --- Service Engineer (Park Side) Subnet ---
 permit 10.56.178.0 0.0.0.255
 remark --- Service Engineer (WTG Side) Subnet ---
 permit 10.57.1.0 0.0.0.255
!
ip access-list extended ACL_CMS_IN
 permit ip any host {{MC_WTG_HOST}}
 deny   ip any 10.60.0.0 0.0.7.255
 permit ip any any
ip access-list extended ACL_SERVICE_TECH_IN
 remark Explicitly allow DHCP requests
 permit udp any eq bootpc any eq bootps
 remark Allow Tech to all services in this turbines MainController
 permit ip 10.57.1.0 0.0.0.255 host {{MC_WTG_HOST}}
 remark Deny Tech to all other turbine services
 deny   ip 10.57.1.0 0.0.0.255 10.60.0.0 0.0.255.255
 permit ip any any
!
ip radius source-interface Vlan{{VL_ID}}
ip sla 1
 icmp-echo {{VL_GATEWAY}} source-interface Vlan{{VL_ID}}
 threshold 2000
 timeout 2000
 frequency 5
ip sla schedule 1 life forever start-time now
logging facility auth
logging source-interface Vlan{{VL_ID}}
logging host 10.56.131.1 transport tcp port 514
!
!
snmp-server community TurbineNet RO ACL_SNMP_RO_ACCESS
snmp-server contact template version {{VER_NUM}}
snmp-server enable traps snmp authentication linkdown linkup coldstart warmstart
snmp-server enable traps config
snmp-server enable traps cpu threshold
snmp-server host 193.47.71.145 TurbineNet
!
radius-server attribute 32 include-in-access-req
radius-server dead-criteria time 4 tries 2
radius-server retransmit 1
radius-server timeout 4
radius-server deadtime 2
!
{{RADIUS_HOSTS}}
!
banner exec %
You have logged on to $(hostname).$(domain) on line $(line)
Template version: $(line-desc)
Region: {{REGION}}
%
!
banner login %
UNAUTHORIZED ACCESS TO THIS DEVICE IS PROHIBITED
You must have explicit, authorized permission to access or configure this device.
Unauthorized attempts and actions to access or use this system may result in civil and/or criminal penalties.
All activities performed on this device are logged and monitored.
%
alias exec wpp-options event manager run wpp-options.tcl
!
line con 0
 location {{COUNTRY_CODE}}-{{SITE_NAME}}-MK3E-NAC-Template{{VER_NUM}}
 logging synchronous
 transport preferred none
line vty 0 4
 location {{COUNTRY_CODE}}-{{SITE_NAME}}-MK3E-NAC-Template{{VER_NUM}}
 access-class ACL_SSH_ALLOWED in
 exec-timeout 15 0
 logging synchronous
 transport preferred none
 transport input ssh
line vty 5 15
 location {{COUNTRY_CODE}}-{{SITE_NAME}}-MK3E-NAC-Template{{VER_NUM}}
 access-class ACL_SSH_ALLOWED in
 exec-timeout 15 0
 logging synchronous
 transport preferred none
 transport input ssh
!
ntp authentication-key 2 md5 0634387449452E3E2D4200181F2A0D2808 7
ntp authenticate
ntp trusted-key 2
ntp server 10.56.139.123 key 2 prefer
event manager environment im_intf FastEthernet1/12
event manager environment options_global1 CMS_v1.1;
event manager directory user policy "flash:/EEM/"
event manager session cli username "eem_user"
event manager policy wtg-islandmode.tcl type user
event manager policy wpp-options.tcl type user
!
wpp-options global CMS_v.1.0
end
